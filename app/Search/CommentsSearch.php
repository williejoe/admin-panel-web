<?php

namespace App\Search;

use Kreait\Firebase\Factory;
use App\Search\SearchResult;

class CommentsSearch extends SearchResult
{
   
    public function final_result(){
        $comments=[];
        $properties=$this->result();
        $this->refrence="users";
        $users=$this->result();
        foreach ($properties as $index => $propertie) {
            if(array_key_exists("comments",$propertie)){
                  $acomments=$propertie["comments"];
                  unset($propertie["comments"]);
                  if(array_key_exists("units",$propertie)){
                     unset($propertie["units"]);
                  }
                  if(array_key_exists("likes",$propertie)){
                    unset($propertie["likes"]);
                  }
                 
                  // complete data

                  $complete="";
                  if(array_key_exists("property_complete_status",$propertie)){
                    if($propertie["property_complete_status"]){
                      $complete="Complete";
                    }else {
                      $complete= "Incomplete";
                    }
                  }
                  else {
                    $complete="Incomplete";
                  }
                  
                  // property status
                  $status='';

                  if (array_key_exists('deleted',$propertie)) {
                    if($propertie['deleted']){
                      $status="Deleted";
                     } 
                     else {
                      $status="Active";
                     }
                  } else {
                      $status="Active";
                  }
                  
                      
                      

                  $count=0;
                    foreach ($acomments as $cindex => $comment) {
                      $comments[]=array_merge($comment, array_merge($propertie,["propertie_id"=>$index,"comment_id"=>$cindex,"comment_by_email"=>$users[$comment["comment_by"]]["email"],"Flag"=>$count,"complete"=>$complete,'current_status'=>$status]));
                      $count=$count+1;
                    }     
              }
        }
        return $comments;
    } 
}
