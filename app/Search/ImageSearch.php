<?php

namespace App\Search;

use Kreait\Firebase\Factory;
use App\Search\SearchResult;

class ImageSearch extends SearchResult
{

    public function Image_result(){
        $result=[];
        $final_result=[];
        $properties=$this->result();
        $this->refrence="users";
        $users=$this->result();
        foreach ($properties as $key => $item) {
            $added_date='';
            if(array_key_exists("date_added",$item)){
              $added_date=$item["date_added"];
            }

            if(array_key_exists("user",$item)){
            if(array_key_exists($item["user"],$users)) {
            $user=$users[$item["user"]];         
            unset($user["token"]);
            if(array_key_exists("prop_notif",$user)){
                unset($user["prop_notif"]);
            }
            $detail=array_merge($user,["properties_id"=>$key,"user_id"=>$item["user"],"latest_added_date"=>$added_date]);
            if(array_key_exists("profileImage",$detail)){
                unset($user["profileImage"]);
            }
            if(array_key_exists("units",$item)){
              if(array_key_exists("rent_rolls",$item["units"][0])){
                $rent_roll=$item["units"][0]["rent_rolls"];

                foreach ($rent_roll as $key1 => $item1) {
                    if($item1!=null){
                        if(array_key_exists("image",$item1) && strlen($item1["image"])>0){
                            $result[]=array_merge(array_merge($detail,["image_index"=>$key1,"location"=>"Property","added_date"=>$item["purchase_date"],"reserve"=>0,"address"=>$item["address"]]),$item1);
                        }
                    }            
                }
              }
            }
            if(array_key_exists("reserver_image",$item)){
                $reserver_image=$item["reserver_image"];
                foreach ($reserver_image as $key2 => $item2) {
                    if($item2!=null){
                        if(array_key_exists("image",$item2) && strlen($item2["image"])>0){
                        $result[]=array_merge(array_merge($detail,["image_index"=>$key2,"location"=>"Deleted Image","reserve"=>1,"added_date"=>$item["purchase_date"],"address"=>$item["address"]]),$item2);
                        }
                    }            
                }
            }
               }     
            }
        }
        foreach ($users as $key => $item) {
            $added_date='';
            if(array_key_exists("date_account_created",$item)){
              $added_date=$item["date_account_created"];
            }
            if(array_key_exists("profileImage",$item) && strlen($item["profileImage"])>0){
                $result[]= array_merge($item,["location"=>"Profile","User_id"=>$key,"latest_added_date"=>$added_date]);   
            }  
        }
        return $result;
    }
 
    
}
