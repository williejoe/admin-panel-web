<?php

namespace App\Search;

use Kreait\Firebase\Factory;
use App\Search\SearchResult;

class ConnectSearch extends SearchResult
{
    public function connection(){
        $factory = (new Factory)
        ->withServiceAccount(__DIR__.'/firebaseKey1.json')
        ->withDatabaseUri('https://realestate-6126a.firebaseio.com/');
        $realtimeDatabase = $factory->createDatabase();
        return $realtimeDatabase;
     }
    public function create_connect($firstname,$lastname,$company,$email,$phone,$website,$country,$state,$city,$description,$profileImage,$profession){
        
        $array = array(0 => 'realtor', 1 => 'wholesaler', 2 => 'property manager', 3 => 'contractor',4=>"lender");
        $key=0;
        if(array_search(strtolower($profession), $array)){
            $key = array_search(strtolower($profession), $array); 
        }
        $professionid=$key+1;
        $realtimeDatabase=$this->connection();
        //$realtimeDatabase->getReference($this->refrence)->remove();
        $reference = $realtimeDatabase->getReference($this->refrence)->push([
            "firstname"=>ucwords(strtolower($firstname)),
            "lastname"=>ucwords(strtolower($lastname)),
            "name"=>ucwords(strtolower($firstname)).' '.ucwords(strtolower($lastname)),
            "company_name"=>ucwords(strtolower($company)),
            "email" => $email,
            "phone" => (string)$phone,
            "website" => $website,
            "country" => $country,
            "state" => $state,
            "city" => $city,
            "description"=>$description,
            "profileImage" => $profileImage,
            "professionName" => ucfirst($profession),
            "professionId"=>(string)$professionid,
            "status" => "2",
            "date_create"=>date('Y-m-d H:i:s'),
            "date_Updated"=>date('Y-m-d H:i:s'),
        ]);
    }

    public function final_result(){
        $resource=$this->result();
        return $resource;
    }
    // public function Image_result(){
    //     $result=[];
    //     $final_result=[];
    //     $properties=$this->result();
    //     $this->refrence="users";
    //     $users=$this->result();

    //     foreach ($properties as $key => $item) {
    //         $user=$users[$item["user"]];
    //         unset($user["token"]);
    //         if(array_key_exists("prop_notif",$user)){
    //             unset($user["prop_notif"]);
    //         }
    //         $detail=array_merge($user,["properties_id"=>$key,"user_id"=>$item["user"]]);
    //         if(array_key_exists("units",$item)){
    //           if(array_key_exists("rent_rolls",$item["units"][0])){
    //             $rent_roll=$item["units"][0]["rent_rolls"];

    //             foreach ($rent_roll as $key1 => $item1) {
    //                 if($item1!=null){
    //                     $result[]=array_merge(array_merge($detail,["image_index"=>$key1,"location"=>"rent_rolls","added_date"=>$item["purchase_date"],"reserve"=>0]),$item1);
    //                 }            
    //             }
    //           }
    //         }
    //         if(array_key_exists("reserver_image",$item)){
    //             $reserver_image=$item["reserver_image"];
    //             foreach ($reserver_image as $key2 => $item2) {
    //                 if($item2!=null){
    //                     $result[]=array_merge(array_merge($detail,["image_index"=>$key2,"location"=>"Deleted Image","reserve"=>1,"added_date"=>$item["purchase_date"]]),$item2);
    //                 }            
    //             }
    //         }
    //     }
       
    //     // dd($result);
    //     return $result;
    // }
 
    
}
