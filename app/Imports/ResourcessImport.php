<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Search\ConnectSearch;

class ResourcessImport implements ToModel,WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        // dd($row);
        $addresource=new ConnectSearch("resource");
        $addresource->create_connect($row["first_name"],$row["last_name"],$row["company_name"],$row["email_address"],$row["phone_number"],$row["website_url"],$row["country"],$row["state_region"],$row["city"],$row["description"],$row["connect_image_url"],$row["profession"]);
       
    }
}


