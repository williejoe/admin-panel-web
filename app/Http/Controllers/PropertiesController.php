<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\PropertiesSearch;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;

class PropertiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getresult(){
        $properties_object1=new PropertiesSearch('properties');
        $pro_array=$properties_object1->Combine_properties();
        return $pro_array;
    }
   
    public function allHeading(){
        return[
          "PropertyID",
          "User",
          "Email",
          "Address",
          "Millis",
          "Prop_type",
          "Property_complete_status",
          "property_status",
          "Account_country",
          "Cash_invested",
          "Key",
          "Purchase_amt",
          "Purchase_date",
          "Annual_hoa",
          "Annual_ins",
          "Annual_mtg",
          "Annual_other",
          "Annual_prom",
          "Annual_prot",
          "Annual_repair",
          "Annual_util",
          "Annual_vac",
          "Bathrooms",
          "Bedrooms",
          "loan_balance",
          "market_value",
          "Month_hoa",
          "Month_ins",
          "Month_mtg",
          "Month_other",
          "Month_prom",
          "Month_prot",
          "Month_repair",
          "Month_util",
          "Month_vac",
          "Mtg_down",
          "Mtg_interest",
          "Mtg_loan",
          "Mtg_purchase",
          "Name",
          "Notes",
          "Rent_annual",
          "Rent_day",
          "Rent_end",
          "Rent_month",
          "Rent_start",
          "Square_feet",
          "Likes", // add
        //   "Status", // add
        ];
      }
    

    public function get_properties(){

        $paginator_obj = new CustomPaginate();
        $data=$paginator_obj->paginate($this->getresult());
        // dd($data);
        $classname="active";
        $url="properties";
        return view('admin.dashboard.properties',compact('data','classname','url'));
    }
    public function get_search(Request $request){
        $text=$request->searchtext;
        $dropdown=$request->searchField;
        if($dropdown=="status"){
            $text=strtolower($text);
        }
        elseif($dropdown=="purchase_date"){
            $text=strtotime($text);
            $text=strval($text);
        }
        $properties_obj=new PropertiesSearch('properties');
        $all_result=$this->getresult();

        $data=$properties_obj->searchAccount($all_result,$text,$dropdown);

        $classname="active";
        $url="searchproperties";
        return view('admin.dashboard.properties',compact('data','classname','url','text','dropdown'));
    }

    public function Update($id){
        $update_array=[];
        $properties_obj=new PropertiesSearch('properties/'.$id);
        $all_result=$properties_obj->result();
      
        if(array_key_exists("deleted",$all_result)){
            if(!$all_result['deleted']){
                $update_array=array_merge($all_result,['deleted'=>true]);
            }
            else{
                $update_array=array_merge($all_result,['deleted'=>false]);
            }
        }
        else{
            $update_array=array_merge($all_result,['deleted'=>true]);
        }
        $properties_obj->updateData($update_array);
        $response = array(
            'status' => 'success',
            'msg' => "succfully update",
        );
        return response()->json($response); 
    }

    public function export(Request $request){
        $allresult=$this->getresult();
        $data=[];
        foreach ($allresult as $key => $item) {
            // dd($item);
            $data[]=[
                "PropertyID"=>  $item["Property_id"],
                "User"=>$item["user"],
                "Email"=> (array_key_exists("email",$item)) ? $item["email"] : "Unavailable",
                "Address"=>$item["address"],
                "Millis"=> (array_key_exists("millis",$item)) ? $item["millis"] : "Unavailable",
                "Prop_type"=> (array_key_exists("prop_type",$item)) ? $item["prop_type"] : "Unavailable",
                "Property_complete_status"=> (array_key_exists("property_complete_status",$item)) ? ($item["property_complete_status"]) ? "true" : "false" : "Unavailable",
                "property_status"=> (array_key_exists("property_status",$item)) ? ($item["property_status"]=="Research") ? "Analysis" : "Owned" : "Unavailable",
                "Account_country"=> (array_key_exists("account_country",$item)) ? $item["account_country"] : "Unavailable",
                "Cash_invested"=>(array_key_exists("cash_invested",$item)) ? $item["cash_invested"] : "Unavailable",
                "Key"=>(array_key_exists("key",$item)) ? $item["key"] : "",
                "Purchase_amt"=>$item["purchase_amt"],
                "Purchase_date"=>$item["purchase_date"],
                "Annual_hoa"=>(array_key_exists("annual_hoa",$item["units"][0])) ? ($item["units"][0]["annual_hoa"]==0) ? "0" : $item["units"][0]["annual_hoa"] : "Unavailable",
                "Annual_ins"=>(array_key_exists("annual_ins",$item["units"][0])) ? ($item["units"][0]["annual_ins"]==0) ? "0" : $item["units"][0]["annual_ins"] : "Unavailable",
                "Annual_mtg"=>(array_key_exists("annual_mtg",$item["units"][0])) ? ($item["units"][0]["annual_mtg"]==0) ? "0" : $item["units"][0]["annual_mtg"] : "Unavailable",
                "Annual_other"=>(array_key_exists("annual_other",$item["units"][0])) ? ($item["units"][0]["annual_other"]==0) ? "0" : $item["units"][0]["annual_other"] : "Unavailable",
                "Annual_prom"=>(array_key_exists("annual_prom",$item["units"][0])) ? ($item["units"][0]["annual_prom"]==0) ? "0" : $item["units"][0]["annual_prom"] : "Unavailable",
                "Annual_prot"=>(array_key_exists("annual_prot",$item["units"][0])) ? ($item["units"][0]["annual_prot"]==0) ? "0" : $item["units"][0]["annual_prot"] : "Unavailable",
                "Annual_repair"=>(array_key_exists("annual_repair",$item["units"][0])) ? ($item["units"][0]["annual_repair"]==0) ? "0" : $item["units"][0]["annual_repair"] : "Unavailable",
                "Annual_util"=>(array_key_exists("annual_util",$item["units"][0])) ? ($item["units"][0]["annual_util"]==0) ? "0" : $item["units"][0]["annual_util"] : "Unavailable",
                "Annual_vac"=>(array_key_exists("annual_vac",$item["units"][0])) ? ($item["units"][0]["annual_vac"]==0) ? "0" : $item["units"][0]["annual_vac"] : "Unavailable",
                "Bathrooms"=>(array_key_exists("bathrooms",$item["units"][0])) ? ($item["units"][0]["bathrooms"]==0) ? "0" : $item["units"][0]["bathrooms"] : "Unavailable",
                "Bedrooms"=>(array_key_exists("bedrooms",$item["units"][0])) ? ($item["units"][0]["bedrooms"]==0) ? "0" : $item["units"][0]["bedrooms"] : "Unavailable",
                
                "loan_balance"=>(array_key_exists("loan_balance",$item["units"][0])) ? ($item["units"][0]["loan_balance"]==0) ? "0" : $item["units"][0]["loan_balance"] : "Unavailable",
                "market_value"=>(array_key_exists("market_value",$item["units"][0])) ? ($item["units"][0]["market_value"]==0) ? "0" : $item["units"][0]["market_value"] : "Unavailable",

                "Month_hoa"=>(array_key_exists("month_hoa",$item["units"][0])) ? ($item["units"][0]["month_hoa"]==0) ? "0" : $item["units"][0]["month_hoa"] : "Unavailable",
                "Month_ins"=>(array_key_exists("month_ins",$item["units"][0])) ? ($item["units"][0]["month_ins"]==0) ? "0" : $item["units"][0]["month_ins"] : "Unavailable",
                "Month_mtg"=>(array_key_exists("month_mtg",$item["units"][0])) ? ($item["units"][0]["month_mtg"]==0) ? "0" : $item["units"][0]["month_mtg"] : "Unavailable",
                "Month_other"=>(array_key_exists("month_other",$item["units"][0])) ? ($item["units"][0]["month_other"]==0) ? "0" : $item["units"][0]["month_other"] : "Unavailable",
                "Month_prom"=>(array_key_exists("month_prom",$item["units"][0])) ? ($item["units"][0]["month_prom"]==0) ? "0" : $item["units"][0]["month_prom"] : "Unavailable",
                "Month_prot"=>(array_key_exists("month_prot",$item["units"][0])) ? ($item["units"][0]["month_prot"]==0) ? "0" : $item["units"][0]["month_prot"] : "Unavailable",
                "Month_repair"=>(array_key_exists("month_repair",$item["units"][0])) ? ($item["units"][0]["month_repair"]==0) ? "0" : $item["units"][0]["month_repair"] : "Unavailable",
                "Month_util"=>(array_key_exists("month_util",$item["units"][0])) ? ($item["units"][0]["month_util"]==0) ? "0" : $item["units"][0]["month_util"] : "Unavailable",
                "Month_vac"=>(array_key_exists("month_vac",$item["units"][0])) ? ($item["units"][0]["month_vac"]==0) ? "0" : $item["units"][0]["month_vac"] : "Unavailable",
                "Mtg_down"=>(array_key_exists("mtg_down",$item["units"][0])) ? ($item["units"][0]["mtg_down"]==0) ? "0" : $item["units"][0]["mtg_down"] : "Unavailable",
                "Mtg_interest"=>(array_key_exists("mtg_interest",$item["units"][0])) ? ($item["units"][0]["mtg_interest"]==0) ? "0" : $item["units"][0]["mtg_interest"] : "Unavailable",
                "Mtg_loan"=>(array_key_exists("mtg_loan",$item["units"][0])) ? ($item["units"][0]["mtg_loan"]==0) ? "0" : $item["units"][0]["mtg_loan"] : "Unavailable",
                "Mtg_purchase"=>(array_key_exists("mtg_purchase",$item["units"][0])) ? ($item["units"][0]["mtg_purchase"]==0) ? "0" : $item["units"][0]["mtg_purchase"] : "Unavailable",
                "Name"=>(array_key_exists("name",$item["units"][0])) ? $item["units"][0]["name"] : "",
                "Notes"=>(array_key_exists("notes",$item["units"][0])) ? $item["units"][0]["notes"] : "",
                "Rent_annual"=>(array_key_exists("rent_annual",$item["units"][0])) ? ($item["units"][0]["rent_annual"]==0) ? "0" : $item["units"][0]["rent_annual"] : "Unavailable",
                "Rent_day"=>(array_key_exists("rent_day",$item["units"][0])) ? ($item["units"][0]["rent_day"]==0) ? "0" : $item["units"][0]["rent_day"] : "Unavailable",
                "Rent_end"=>(array_key_exists("rent_end",$item["units"][0])) ? ($item["units"][0]["rent_end"]==0) ? "0" : $item["units"][0]["rent_end"] : "Unavailable",
                "Rent_month"=>(array_key_exists("rent_month",$item["units"][0])) ? ($item["units"][0]["rent_month"]==0) ? "0" : $item["units"][0]["rent_month"] : "Unavailable",
                "Rent_start"=>(array_key_exists("rent_start",$item["units"][0])) ? ($item["units"][0]["rent_start"]==0) ? "0" : $item["units"][0]["rent_start"] : "Unavailable",
                "Square_feet"=>(array_key_exists("square_feet",$item["units"][0])) ? ($item["units"][0]["square_feet"]==0) ? "0" : $item["units"][0]["square_feet"] : "Unavailable",
                "Likes"=>(array_key_exists("likes",$item)) ? $item["likes"] : "Unavailable",
                // "Status"=>(array_key_exists("deleted",$item)) ? ($item["deleted"]) ? "Deleted" : "Active" : "Active",
            ];
        }
        return Excel::download(new AccountsExport($data,$this->allHeading()), 'properties.csv');
       
    }

   public function propertyStatus(Request $request,$id){
      $update_array=[];
      $properties_obj=new PropertiesSearch('properties/'.$id);
      $all_result=$properties_obj->result();
      $all_result["property_visible_status"]="forsale";
      $all_result["date_last_updated"]=date('Y-m-d');
    //   $properties_obj->updateData($all_result);
      $response = array(
        //   "id"=>$id,
          'result'=>$all_result,
          'status' => 'success',
          'msg' => "succfully update",
       );
      return response()->json($response);
   }

   public function showproperty($id){
       $update_array=[];
       $properties_obj=new PropertiesSearch('properties/'.$id);
       $d_data=$properties_obj->result();
       $user_obj=new PropertiesSearch('users/'.$d_data["user"]);
       $user_data=$user_obj->result();
       $data=array_merge($d_data,["email"=>$user_data["email"]]);
       $classname="active";
       return view('admin.dashboard.propertydetail',compact('data','classname','id'));
   }
   public function updateProperties(Request $request,$id){
        $update_array=[];
        $properties_obj=new PropertiesSearch('properties/'.$id);
        $d_data=$properties_obj->result();
        if($request->hasFile('profileImage') )
        {
            $d_data["property_image"] = $request->file_path;
        }
        $d_data["property_desc"]=$request->property_desc;
        $d_data["address"]=$request->address;
        $d_data["date_added"]=$request->date_added == null ? date('Y-m-d') : $request->date_added;
        $d_data["property_status"]=$request->property_status;
        $d_data["property_complete_status"]=$request->property_complete_status=="Complete" ? true : false;
        $d_data["date_last_updated"]=$request->date_last_updated == null ? date('Y-m-d') : $request->date_last_updated;
        $d_data["prop_type"]=$request->prop_type;
        $d_data["purchase_amt"]=(int)$request->purchase_amt;
        $d_data["down_payment"]=(int)$request->down_payment;
        $d_data["closing_cost"]=(int)$request->closing_cost;
        $d_data["rehab_cost"]=(int)$request->rehab_cost;
        $d_data["units"][0]["market_value"]=(int)$request->market_value;
        $d_data["units"][0]["bedrooms"]=(int)$request->bedrooms;
        $d_data["units"][0]["bathrooms"]=(int)$request->bathrooms;
        $d_data["units"][0]["square_feet"]=(int)$request->square_feet;
        $d_data["units"][0]["rent_month"]=(int)$request->rent_month;
        $d_data["units"][0]["rent_annual"]=(int)$request->rent_annual;
        $d_data["units"][0]["month_ins"]=(int)$request->month_ins;
        $d_data["units"][0]["annual_ins"]=(int)$request->annual_ins;
        $d_data["units"][0]["month_prot"]=(int)$request->month_prot;
        $d_data["units"][0]["annual_prot"]=(int)$request->annual_prot;
        $d_data["units"][0]["month_vac"]=(int)$request->month_vac;
        $d_data["units"][0]["annual_vac"]=(int)$request->annual_vac;
        $d_data["units"][0]["month_repair"]=(int)$request->month_repair;
        $d_data["units"][0]["annual_repair"]=(int)$request->annual_repair;
        $d_data["units"][0]["month_prom"]=(int)$request->month_prom;
        $d_data["units"][0]["annual_prom"]=(int)$request->annual_prom;
        $d_data["units"][0]["month_util"]=(int)$request->month_util;
        $d_data["units"][0]["annual_util"]=(int)$request->annual_util;
        $d_data["units"][0]["month_hoa"]=(int)$request->month_hoa;
        $d_data["units"][0]["annual_hoa"]=(int)$request->annual_hoa;
        $d_data["units"][0]["month_other"]=(int)$request->month_other;
        $d_data["units"][0]["annual_other"]=(int)$request->annual_other;
        $d_data["units"][0]["mtg_interest"]=(int)$request->mtg_interest;
        $d_data["units"][0]["loan_balance"]=(int)$request->loan_balance;
        $d_data["units"][0]["capex_month"]=(int)$request->capex_month;
        $d_data["units"][0]["capex_annual"]=(int)$request->capex_annual;
        $d_data["units"][0]["depreciation_month"]=(int)$request->depreciation_month;
        $d_data["units"][0]["depreciation_year"]=(int)$request->depreciation_year;
        $d_data["units"][0]["month_other"]=(int)$request->month_other;
        $d_data["units"][0]["mtg_loan"]=(int)$request->mtg_loan;
        $d_data["units"][0]["mtg_interest_month"]=(int)$request->mtg_interest_month;
        $d_data["units"][0]["mtg_interest_annual"]=(int)$request->mtg_interest_annual;
        $properties_obj->updateData($d_data);
        $classname="active";
        $user_obj=new PropertiesSearch('users/'.$d_data["user"]);
        $user_data=$user_obj->result();
        $data=array_merge($d_data,["email"=>$user_data["email"]]);
        return view('admin.dashboard.propertydetail',compact('data','classname','id'));
   }

}
