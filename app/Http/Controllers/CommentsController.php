<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\CommentsSearch;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getresult(){
        $comments=new CommentsSearch('properties');
        return $comments->final_result();
    }

    public function allHeading(){
        return[
          "Property ID",
          "Property Status",
          "Complete Data", 
          "OwnerShip Type",
          "Property Address",
          "Comment", 
          "Commenter Name",  
          "Commenter Email",
          "Date Commented", 
          "Flag Count", 
          "Comment Status",
        ];
      }

    public function index(Request $request){
        $request->session()->put('searchtext', '');
        $request->session()->put('search', "all");
        $final_result=$this->getresult();
        // dd($final_result);
        $paginator_obj = new CustomPaginate();
        $data=$paginator_obj->paginate($final_result);
        $classname="active";
        $url="comments";
        $sessiondrop=$request->session()->get('search');
        $sessiontext=$request->session()->get('searchtext');
        return view('admin.dashboard.commentspanel',compact('data','classname','url','sessiondrop','sessiontext'));
    }
    public function search(Request $request){
        $text=$request->searchtext;
        $dropdown=$request->searchField;
        if ($dropdown=="property_complete_status") {
            $text=true;
        }
        if ($dropdown=="deleted") {
            dd("deleted");
        }
        if($dropdown=="all"){
         $text='';
        }
        $request->session()->put('search', $dropdown);
        $request->session()->put('searchtext', $text);
        $classname="active";
        $url="comments";
        $sessiondrop='';
        $sessiontext='';
        if ($request->session()->has('search') && $request->session()->has('searchtext')) {
            $sessiondrop=$request->session()->get('search');
            $sessiontext=$request->session()->get('searchtext');
        }
        else{
            $sessiondrop="all";
        }
        if($dropdown=="all"){
            $data= $this->getresult();
            return view('admin.dashboard.commentspanel',compact('data','classname','url','text','dropdown','sessiondrop','sessiontext'));
        }
        $result=$this->getresult();
        $img_obj=new CommentsSearch("properties");
        $data=$img_obj->searchAccount($result,$text,$dropdown);
        
        return view('admin.dashboard.commentspanel',compact('data','classname','url','text','dropdown','sessiondrop','sessiontext'));
    }

   

    public function export(Request $request){
        $result=$this->getresult();
        $data=[];
        foreach ($result as $key => $item) {
            // dd($item);
            $data[]=[
                "Property_ID"=> (array_key_exists("propertie_id",$item)) ? $item["propertie_id"] : "Unavailable",
                "Property_Status"=> (array_key_exists("deleted",$item)) ? ($item["deleted"]) ? "Deleted" : "Active" : "Active",
                "Complete_Data"=> (array_key_exists("property_complete_status",$item)) ? ($item["property_complete_status"]) ? "Complete" : "Incomplete" : "Complete",
                "OwnerShip_Type"=> (array_key_exists("property_status",$item)) ? $item["property_status"] : "Unavailable",
                "Property_Address"=> (array_key_exists("address",$item)) ? $item["address"] : "Unavailable",
                "Comment"=> (array_key_exists("comment_desc",$item)) ? $item["comment_desc"] : "Unavailable",
                "Commenter_Name"=> (array_key_exists("comment_by_name",$item)) ? $item["comment_by_name"] : "Unavailable",
                "Commenter_Email"=> (array_key_exists("comment_by_email",$item)) ? $item["comment_by_email"] : "Unavailable",
                "Date_Commented"=> (array_key_exists("comment_date",$item)) ? $item["comment_date"] : "Unavailable",
                "Flag_Count"=> (array_key_exists("Flag",$item)) ? $item["Flag"] : "Unavailable",
                "Comment_Status"=> (array_key_exists("is_deleted",$item)) ? ($item["is_deleted"]) ? "Deleted" : "Active" : "Active",
            ];
        }
         return Excel::download(new AccountsExport($data,$this->allHeading()), 'comments.csv');
    }

  
    public function Update(Request $request,$id){
        $update_array=[];
        $accout_object=new CommentsSearch('properties/'.$id);
        $all_result=$accout_object->result();
        if($all_result["comments"][$request->commentid]["is_deleted"]){
            $all_result["comments"][$request->commentid]["is_deleted"]=false;
        }
        else{
            $all_result["comments"][$request->commentid]["is_deleted"]=true;
        }
          
      $accout_object->updateData($all_result);
      $response = array(
          "property"=>$all_result["comments"][$request->commentid],
          'status' => 'success',
          'msg' => "succfully update",
      );
      return response()->json($response); 
    }
    
}
