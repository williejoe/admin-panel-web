<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\SearchResult;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;

class FirebaseAccess extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function allHeading(){
    return[
      "User_id",
      "Email",
      "Name",
      "Username",
      "Token",
      "Currency",
      "Phone_number",
      "Platform",
      "Version",
      "date_account_created",
      "last_date_active",
      "Properties",
      "Status",
    ];
  }
  public static function date_compare($a, $b){
     if(array_key_exists('date_account_created',$a) && array_key_exists('date_account_created',$b)){
          $arr1=explode("-",$a["date_account_created"]);
          $arr2=explode("-",$b["date_account_created"]);
          $date1=$arr1[2]."-".$arr1[0]."-".$arr1[1];
          $date2=$arr2[2]."-".$arr2[0]."-".$arr2[1];
          $a = strtotime($date1);
          $b = strtotime($date2);

          if ($a == $b) {
              return 0;
          }
          return ($a < $b) ? 1 : -1;
     }
     else{
          return 0;
     }
     
  }

 public function getrecord(){
    $accout_object=new SearchResult('accounts');
    $final_result=$accout_object->final_result();
    usort($final_result, 'static::date_compare');
    $paginator_obj = new CustomPaginate();
    $data=$paginator_obj->paginate($final_result);
    $classname="active";
    $url="dashboard";
    return view('admin.dashboard.adminPanel',compact('data','classname','url'));
 }   
 
 public function searchResult(Request $request){
   $text=$request->searchtext;
   $dropdown=$request->searchField;
   $accout_object=new SearchResult('accounts');
   $final_result=$accout_object->final_result();
   $data=$accout_object->searchAccount($final_result,$request->searchtext,$request->searchField);
  //  $paginator_obj = new CustomPaginate();
  //  $data=$paginator_obj->paginate($accounts);
   $classname="active";
   $url="searchaaccount";
   return view('admin.dashboard.adminPanel',compact('data','classname','url','text','dropdown'));
 }

    public function export(Request $request){
      
        $accout_object=new SearchResult('accounts');
        $final_result=$accout_object->final_result();
        $data=[];
        foreach ($final_result as $key => $item) {
          //  dd($item);
            $data[]=[
                "User_id"=>  $item["User_id"],
                "Email"=>(array_key_exists("email",$item)) ? $item["email"] : "Unavailable",
                "Name"=> (array_key_exists("name",$item)) ? $item["name"] : "Unavailable",
                "username"=> (array_key_exists("username",$item)) ? $item["username"] : "Unavailable",
                "Token"=> (array_key_exists("token",$item)) ? $item["token"] : "Unavailable",
                "Currency"=> (array_key_exists("currency",$item)) ? $item["currency"] : "Unavailable",
                "Phone_number"=> (array_key_exists("phone_number",$item)) ? $item["phone_number"] : "Unavailable",
                "Platform"=> (array_key_exists("OS",$item)) ? $item["OS"] : "Unavailable",
                "version"=>  (array_key_exists("version",$item)) ? $item["version"] : "Unavailable",
                "date_account_created"=>  (array_key_exists("date_account_created",$item)) ? $item["date_account_created"] : "Unavailable",
                "last_date_active"=>  (array_key_exists("last_date_active",$item)) ? $item["last_date_active"] : "Unavailable",
                "Properties"=>(array_key_exists("properties",$item)) ? $item["properties"] : "Unavailable",
                "Status"=>(array_key_exists("notif",$item)) ?  ($item["notif"]) ? "Deleted" : "Active" : "Active",
            ];
        }
      return Excel::download(new AccountsExport($data,$this->allHeading()), 'accounts.csv');
   
    }

    public function Update($id){
        $update_array=[];
        $user_obj=new SearchResult('users/'.$id);
        $all_result=$user_obj->result();
      
        if(array_key_exists("notif",$all_result)){
            if(!$all_result['notif']){
                $update_array=array_merge($all_result,['notif'=>true,"prop_notif"=>true]);
            }
            else{
                $update_array=array_merge($all_result,['notif'=>false,"prop_notif"=>false]);
            }
        }
          else{
              $update_array=array_merge($all_result,['notif'=>true,"prop_notif"=>true]);
          }
        
      
        $user_obj->updateData($update_array);
        $response = array(
          'status' => 'success',
          'msg' => "succfully update",
      );
      return response()->json($response); 
    }


}
