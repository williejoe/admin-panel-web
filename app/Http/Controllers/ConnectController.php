<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search\ConnectSearch;
use App\Search\ConnectsSearchResult;
use App\CusPagination\CustomPaginate;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AccountsExport;
use App\Exports\SearchExport;
use App\Imports\ResourcessImport;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;



class ConnectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getresult(){
        $accout_object=new ConnectSearch('resource');
        return $accout_object->final_result();
    }

    public function allHeading(){
        return[
          "city",
          "company_name", // add
          "country",
          "date_Updated", // add
          "date_create", // add
          "description",  // add
          "email",
          "firstname", // add 
          "lastname", // add
          "name",
          "inSearchCount", // add 
          "learnMoreCount", // add
          "likes", // add
          "phone",
          "professionId",
          "professionName",
          "profileImage",
          "ratings_average", // add
          "reviews_count", // add
          "state",
          "status",
          "visibleCount", // add
          "visitWebSiteCount", // add
          "website"
        ];
      }

    public function index(Request $request){

        $request->session()->put('searchtext', '');
        $request->session()->put('search', "all");
        $final_result=$this->getresult();
       
        $paginator_obj = new CustomPaginate();
        $data=$paginator_obj->paginate($final_result);
        $classname="active";
        $url="connects";
        $sessiondrop=$request->session()->get('search');
        $sessiontext=$request->session()->get('searchtext');
        return view('admin.dashboard.connectsPanel',compact('data','classname','url','sessiondrop','sessiontext'));
    }
    public function detail($id){
        $accout_object=new ConnectSearch('resource/'.$id);
        $data=$accout_object->final_result();

        // dd($data);
        $classname="active";
        $url="connects";
        return view('admin.dashboard.connectdetail',compact('data','classname','url','id'));
    }
    public function search(Request $request){
        $text=$request->searchtext;
        $dropdown=$request->searchField;
        if($dropdown=="all"){
         $text='';
        }
        $request->session()->put('search', $dropdown);
        $request->session()->put('searchtext', $text);
        $classname="active";
        $url="searchimage";
        $sessiondrop='';
        $sessiontext='';
        if ($request->session()->has('search') && $request->session()->has('searchtext')) {
            $sessiondrop=$request->session()->get('search');
            $sessiontext=$request->session()->get('searchtext');
          }
          else{
            $sessiondrop="all";
          }
        if($dropdown=="userid"){
            $data=[];
            $accout_object=new ConnectSearch('resource/'.$text);
            $single_result=$accout_object->final_result();
            if($single_result!=null){
                $data[]= array_merge($single_result,["U_id"=>$text]);
            }
            return view('admin.dashboard.connectsPanel',compact('data','classname','url','text','dropdown','sessiondrop','sessiontext'));
        }elseif($dropdown=="all"){
            $data= $this->getresult();
            return view('admin.dashboard.connectsPanel',compact('data','classname','url','text','dropdown','sessiondrop','sessiontext'));
        }
        
        $result=$this->getresult();
        $img_obj=new ConnectSearch("resource");
        $data=$img_obj->searchAccount($result,$text,$dropdown);
        
        return view('admin.dashboard.connectsPanel',compact('data','classname','url','text','dropdown','sessiondrop','sessiontext'));
    }

    public function statussearch($status){
        $dropdown="status";
        $result=$this->getresult();
        // dd($result);
        
        $img_obj=new ConnectSearch("resource");
        $data=$img_obj->searchAccount($result,$status,$dropdown);
        $response='';
        if($status=="select"){
            $response = array(
                'status' => 'success',
                "data"=>$result,
            );
        }
        else{
            $response = array(
                'status' => 'success',
                "data"=>$data,
            );
        }
        
        return response()->json($response); 
     
        $classname="active";
        $url="searchimage";
        return view('admin.dashboard.connectsPanel',compact('data','classname','url','text','dropdown'));
    }

    public function export(Request $request){
        $result=$this->getresult();
        $data=[];
        foreach ($result as $key => $item) {
            // dd($item);
            $data[]=[
                "city"=> (array_key_exists("city",$item)) ? $item["city"] : "Unavailable",
                "company_name"=> (array_key_exists("company_name",$item)) ? $item["company_name"] : "Unavailable",
                "country"=> (array_key_exists("country",$item)) ? $item["country"] : "Unavailable",
                "date_Updated"=> (array_key_exists("date_Updated",$item)) ? $item["date_Updated"] : "Unavailable",
                "date_create"=> (array_key_exists("date_create",$item)) ? $item["date_create"] : "Unavailable",
                "description"=> (array_key_exists("description",$item)) ? $item["description"] : "Unavailable",
                "email"=> (array_key_exists("email",$item)) ? $item["email"] : "Unavailable",
                "firstname"=> (array_key_exists("firstname",$item)) ? $item["firstname"] : "Unavailable",
                "lastname"=> (array_key_exists("lastname",$item)) ? $item["lastname"] : "Unavailable",
                "name"=> (array_key_exists("name",$item)) ? $item["name"] : "Unavailable",
                "inSearchCount"=> (array_key_exists("inSearchCount",$item)) ? $item["inSearchCount"] : "Unavailable",
                "learnMoreCount"=> (array_key_exists("learnMoreCount",$item)) ? $item["learnMoreCount"] : "Unavailable",
                "likes"=> (array_key_exists("likes",$item)) ? count($item["likes"]) : "Unavailable",
                "phone"=> (array_key_exists("phone",$item)) ? $item["phone"] : "Unavailable",
                "professionId"=> (array_key_exists("professionId",$item)) ? $item["professionId"] : "Unavailable",
                "professionName"=> (array_key_exists("professionName",$item)) ? $item["professionName"] : "Unavailable",
                "profileImage"=> (array_key_exists("profileImage",$item)) ? $item["profileImage"] : "Unavailable",
                "ratings_average"=> (array_key_exists("ratings_average",$item)) ? $item["ratings_average"] : "Unavailable",
                "reviews_count"=> (array_key_exists("reviews_count",$item)) ? $item["reviews_count"] : "Unavailable",
                "state"=> (array_key_exists("state",$item)) ? $item["state"] : "Unavailable",
                "status"=> (array_key_exists("status",$item)) ?  ($item["status"]=="2") ? "Approved" : "Disabled" : "Unavailable",
                "visibleCount"=> (array_key_exists("visibleCount",$item)) ? $item["visibleCount"] : "Unavailable",
                "visitWebSiteCount"=> (array_key_exists("visitWebSiteCount",$item)) ? $item["visitWebSiteCount"] : "Unavailable",
                "website"=> (array_key_exists("website",$item)) ? $item["website"] : "Unavailable"
            ];
        }
         return Excel::download(new AccountsExport($data,$this->allHeading()), 'resources.csv');
    }

    public function import(){
        // dd($request->all());
        Excel::import(new ResourcessImport,request()->file('importfile'));
        return redirect()->route('connects')->with('status', 'Profile updated!');
    }


    public function Update($id){
        $update_array=[];
        $accout_object=new ConnectSearch('resource/'.$id);
        $all_result=$accout_object->final_result();
      
        if(array_key_exists("status",$all_result)){
            if(strtolower($all_result["status"])=="2"){
                $update_array=array_merge($all_result,['status'=>"1","date_Updated"=>date('Y-m-d H:i:s')]);
            }
            else{
                $update_array=array_merge($all_result,['status'=>"2","date_Updated"=>date('Y-m-d H:i:s')]);
            }
        }
        else{
            $update_array=array_merge($all_result,['status'=>"2","date_Updated"=>date('Y-m-d H:i:s')]);
        }
          
        $accout_object->updateData($update_array);
        $response = array(
          'status' => 'success',
          'msg' => "succfully update",
      );
      return response()->json($response); 
    }
    public function updateDetail(Request $request,$id){
        // dd($request->all());
        $accout_object=new ConnectSearch('resource/'.$id);
        $all_result=$accout_object->final_result();
        $user_image='';
        $url='';
        if($request->hasFile('profileImage') )
        {
           $url=  $request->file_path;
        }
        else{
            if (array_key_exists("profileImage",$all_result)) {
                $url=$all_result["profileImage"];
            }
        }
        $all_result["firstname"]=ucwords(strtolower($request->firstname));
        $all_result["lastname"]=ucwords(strtolower($request->lastname));
        $all_result["name"] = ucwords(strtolower($request->firstname))." ".ucwords(strtolower($request->lastname));
        $all_result["company_name"]=ucwords(strtolower($request->company_name));
        $all_result["email"]=$request->email;
        $all_result["phone"]=$request->phone;
        $all_result["website"]=$request->website;
        $all_result["country"]=$request->country;
        $all_result["state"]=$request->state;
        $all_result["city"]=$request->city;
        $all_result["description"]=$request->description;
        $all_result["profileImage"]=$url;
        $all_result["professionName"]=$request->professionName;
        $all_result["date_Updated"]= date('Y-m-d H:i:s');
   
        $accout_object->updateData($all_result);
        return redirect()->route('detail',$id)->with(["status"=>"Added"]);;
    
    }
}
