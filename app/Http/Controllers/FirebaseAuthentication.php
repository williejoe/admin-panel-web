<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Auth;
use App\User;
class FirebaseAuthentication extends Controller
{
    protected $auth;
    public function LoginForm(Request $request){
        if($request->session()->has('email')){
            // dd($request->session()->get('email'));
            return redirect()->route('dashboard');
        }
        return view('admin.auth.login');
    }

   public function Login(Request $request){
       $factory = (new Factory)->withServiceAccount(__DIR__.'/firebaseKey1.json')->withDatabaseUri('https://realestate-6126a.firebaseio.com/');
       $auth = $factory->createAuth();
       $signInResult = $auth->signInWithEmailAndPassword($request->email, $request->password);
       if($signInResult){
        $request->session()->put('email', $signInResult->data()['email']);
        return redirect()->route('dashboard');
       }
      
       return view('admin.auth.login');
    }

    public function logout(Request $request) {
        $request->session()->forget('email');
        return redirect()->route('loginform');
    }
}
