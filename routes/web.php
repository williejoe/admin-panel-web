<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});



// Route::get('/login', 'FirebaseAuthentication@LoginForm')->name('loginform');
// Route::post('/login', 'FirebaseAuthentication@Login')->name('login');
// Route::get('/logout', 'FirebaseAuthentication@logout')->name('logout');


Route::get('/dashboard','FirebaseAccess@getrecord' )->name('dashboard');
Route::any('/searchaaccount','FirebaseAccess@searchResult' )->name('searchaccount');
Route::post('/accountExport','FirebaseAccess@export' )->name('accountExport');
Route::get('/updateuser/{id}','FirebaseAccess@Update' )->name('updateuser');

Route::get('/properties','PropertiesController@get_properties' )->name('properties');
Route::any('/searchproperties','PropertiesController@get_search' )->name('searchproperties');
Route::get('/updateproperties/{id}','PropertiesController@Update' )->name('updateproperties');
Route::get('/propertiedetail/{id}','PropertiesController@showproperty' )->name('propertiedetail');
Route::get('/propertystatus/{id}','PropertiesController@propertyStatus' )->name('propertystatus');
Route::post('/propertiesExport','PropertiesController@export' )->name('propertiesExport');
Route::post('/updateproperties/{id}','PropertiesController@updateProperties' )->name('updateproperties');


Route::get('/images','ImageController@index' )->name('images');
Route::post('/searchimage','ImageController@search' )->name('searchimage');
Route::post('/imagesExport','ImageController@export' )->name('imagesExport');
Route::get('/deleteimage/{id}','ImageController@delete' )->name('deleteimage');
Route::get('/reverseImage/{id}','ImageController@reverse' )->name('reverseImage');

// updateconnect

Route::get('/connects','ConnectController@index' )->name('connects');
Route::get('/connects/{status}','ConnectController@statussearch' )->name('statussearch');
Route::post('/searchconnect','ConnectController@search' )->name('searchconnect');
Route::get('/updateconnect/{id}','ConnectController@Update' )->name('updateconnect');
Route::post('/connectsExport','ConnectController@export' )->name('connectsExport');
Route::post('/connectsImport','ConnectController@import' )->name('connectsImport');
Route::get('detail/{id}','ConnectController@detail')->name('detail');
Route::post('updateconnect/{id}','ConnectController@updateDetail')->name('updateDetail');


// commentsroute
Route::get('/comments','CommentsController@index' )->name('comments');
Route::post('/searchcomments','CommentsController@search' )->name('searchcomments');
Route::post('/commentExport','CommentsController@export' )->name('commentExport');
Route::get('/deletecomment/{id}','CommentsController@Update' )->name('deletecomment');

Auth::routes(['register' => false]);
// ['register' => false];
Route::get('/home', 'HomeController@index')->name('home');
