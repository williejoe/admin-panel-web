@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass {{$classname}}"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass"><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass"><span>Connects</span></a>
          <a href="{{route('comments')}}" class="buttonClass"><span>Comments</span></a>
         </div>
         <div class="search__container">
          <div>
           <form action="{{route('searchaccount')}}" method="post">
             @csrf
            <input type="text" name="searchtext" placeholder="Search" value="@isset($text){{ $text }}@endisset">
            <select name="searchField" id="cars">
            <option value="@isset($dropdown){{ $dropdown }}@endisset">Select</option>
            <option value="name">Name</option>
            <option value="email">Email</option>
            <option value="username">Username</option>
            <option value="date_account_created">Created Date</option>
            <option value="last_date_active">Updated Date</option>
          
            </select>
            <button type="submit" class="search__button">Search</button>
           </form>
          </div>

          <div class="export__element">
              @if ($url==="dashboard")
              <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url(1)}}"><<</a>
              <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
              <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
              <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->lastPage())}}">>></a>
              @endif
              <form action="{{route('accountExport')}}" method="post">
                 @csrf
                <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                <button class="search__button export_button" type="submit">Export</button>
              </form>
          </div>
           
         </div>
 
  
          <div id="mytable" class="dataTables_wrapper dt-bootstrap4">
              <div class="row">
                <div class="col-sm-12">
              <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Date Account Created</th>
                  <th>Last Date Active</th>
                  <th>Property Count</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                @php
                    $start = strtotime("01 January 2021");
                    $end = strtotime("31 December 2021");
                    $timestamp = mt_rand($start, $end);
                    $created_date=date("m/d/Y", $timestamp);
                    $start_login=strtotime($created_date);
                    $login_timestamp = mt_rand($start_login, $end);
                    $last_login=date("m/d/Y", $login_timestamp);
    // dd($created_date);
                   @endphp
                   <script>
                     function convertDate(d){
                        var parts = d.split(" ");
                        var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
                        return parts[3]+"-"+months[parts[1]]+"-"+parts[2];
                      }

                   </script>
                <tr>
                  <td>  @php
                    if (array_key_exists("name",$item)){
                      echo $item['name'];
                      }
                 @endphp</td>
                  <td>
                    @php
                    if (array_key_exists("email",$item)){
                        echo $item['email'];
                      }
                 @endphp
                 </td>
                 <td>
                  @php
                  if (array_key_exists("username",$item)){
                      echo $item['username'];
                    }
                  @endphp
                </td>
                <td>
                  @php
                  if (array_key_exists("date_account_created",$item)){
                    echo $item['date_account_created'];
                    }
                  @endphp
                </td>
                <td>
                  @php
                  if (array_key_exists("last_date_active",$item)){
                    echo $item['last_date_active'];
                  }
                  @endphp
                </td>
                  <td>@php
                      if (array_key_exists("properties",$item)){
                        echo $item['properties'];
                      }
                  @endphp</td>
                  <td id="user-{{$item['User_id']}}">@php
                    if (array_key_exists("notif",$item)){
                      foreach ($item as $key => $value) {
                           if($key==="notif"){
                               if($item[$key]){
                                echo "Deleted";
                               } 
                               else {
                                echo "Active";
                               }
                           }
                    }
                    }
                    else {
                        echo 'Active';
                    }
                    
                @endphp</td>
                  <td> <button class="btn btn-link" id="userbutton-{{$item['User_id']}}" onclick="handleaction('{{$item['User_id']}}')">
                    @php
                      if (array_key_exists("notif",$item)){
                        foreach ($item as $key => $value) {
                             if($key==="notif"){
                                 if($item[$key]){
                                    echo 'Reverse'; 
                                 } 
                                 else {
                                    echo "Delete";
                                 }
                             }
                      }
                      }
                      else {
                          echo 'Delete';
                      }
                  @endphp
                  </button></td>
                </tr>
                @endforeach
             
               
              </tbody>
            
            </table>
          </div>
        </div>
      </div>

       

      
@endsection

@push('page_scripts')
<script>

    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }

    const handleaction=(id)=>{
      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`user-${id}`);
      const statusButton=document.getElementById(`userbutton-${id}`);
      
      const xhr=new XMLHttpRequest();
      const url=`/updateuser/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
          const myresponse=JSON.parse(this.responseText);
          if(myresponse.status=="success"){
            if(statusdocument.innerText=="Deleted"){
            statusdocument.innerText="Active";
            statusButton.innerText="Delete";
          }
          else{
            statusdocument.innerText="Deleted";
            statusButton.innerText="Reverse";
          }
          loadercontainer.classList.remove('active');
        }
      }
      xhr.send(); 
    }
    
    var date = new Date((1525227766000));//data[k].timestamp
    console.log(date);



</script>

@endpush