@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
        <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass "><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass {{$classname}}"><span>Connects</span></a>
          <a href="{{route('comments')}}" class="buttonClass"><span>Comments</span></a>
         </div>
 
         <div>
       
          <!-- /.card-header -->
          <div>
              <div style="display: flex;margin-bottom:20px;align-items:center">
                <a href="{{route("connects")}}" class="btn btn-secondary mr-3">Back</a>
                <img src="@php
                      if(array_key_exists("profileImage",$data)){
                           echo $data["profileImage"];
                     }
                     else {
                         echo "";
                     }
                @endphp" alt="" style="width: 200px;height:200px:border-radius:10px">
                <button class="btn btn-success ml-3 @if($data["status"]=="2") {{"d-none"}} @endif" onclick="handleaction('{{$id}}')">Enabled</button>
                <button class="btn btn-danger ml-3 @if($data["status"]=="1") {{"d-none"}} @endif"  onclick="handleaction('{{$id}}')">Disabled</button>
              </div>
            @if(session()->has('status'))
              <div style="width: 50%;margin:auto">
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> Message!</h5>
                  Successfully updated
                </div>
            </div>
             @endif
            <table style="width: 70%">
              <tbody>
                <form action="{{route('updateDetail',$id)}}" method="post" enctype="multipart/form-data">
                  @csrf
                <tr style="height: 30px">
                  <td style="border: 1px solid black">
                   status 
                  </td>
                  <td style="border: 1px solid black" id="user-{{$id}}">
                    @php
                        if($data["status"]=="Approved"){
                            echo "Enable";
                        }
                        else{
                            echo "Disable";
                        }
                    @endphp
                  </td>
                </tr>
                <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Date Added 
                    </td>
                    <td style="border: 1px solid black">
                      @php
                     if(array_key_exists("date_create",$data)){
                           $date = date_create($data["date_create"]);
                            echo date_format($date,'Y/m/d');
                     }
                     else {
                         echo date('Y/m/d');
                     }
                      @endphp
                    
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Last Updated 
                    </td>
                    <td style="border: 1px solid black">
                      @php
                      if(array_key_exists("date_Updated",$data)){
                        $date = date_create($data["date_Updated"]);
                            echo date_format($date,'Y/m/d');
                      }
                      else {
                          echo date('Y/m/d');
                      }
                      
                       @endphp
                   
                    </td>
                  </tr>
              </tbody>
            </table>
         
            <table style="width: 70%;margin-top:30px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Connect User ID 
                    </td>
                    <td style="border: 1px solid black">
                       {{$id}}
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     firstname 
                    </td>
                    <td style="border: 1px solid black">
                    <input style="border: none;width:100%" type="text" name="firstname" id="" value="@if(array_key_exists("firstname",$data)){{$data["firstname"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                       Last name
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="lastname" id="lastname" value="@if(array_key_exists("lastname",$data)){{$data["lastname"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                       Company Name 
                      </td>
                      <td style="border: 1px solid black">
                      <input style="border:none;width:100%" type="text" name="company_name" id="company_name" value="@if(array_key_exists("company_name",$data)){{$data["company_name"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Email
                        </td>
                        <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="email" name="email" id="email" value="@if(array_key_exists("email",$data)){{$data["email"]}}@endif">
                        </td>
                      </tr>

                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Phone Number 
                        </td>
                        <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="phone" id="phone" value="@if(array_key_exists("phone",$data)){{$data["phone"]}}@endif">
                        </td>
                      </tr>
                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Website Url
                        </td>
                        <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="website" id="website" value="@if(array_key_exists("website",$data)){{$data["website"]}}@endif">
                        </td>
                      </tr>
                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Country 
                        </td>
                        <td style="border: 1px solid black">
                           <input style="border: none;width:100%" type="text" name="country" id="country" value="@if(array_key_exists("country",$data)){{$data["country"]}}@endif">
                        </td>
                      </tr>
                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         State_region 
                        </td>
                        <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="state" id="state" value="@if(array_key_exists("state",$data)){{$data["state"]}}@endif">
                        </td>
                      </tr>
                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         City
                        </td>
                        <td style="border: 1px solid black">
                          <input style="border: none;width:100%" type="text" name="city" id="city" value="@if(array_key_exists("city",$data)){{$data["city"]}}@endif">
                        </td>
                      </tr>

                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Description
                        </td>
                        <td style="border: 1px solid black">
                          <textarea style="width: 100%;border:none" rows="4" type="text" name="description" id="description" value="@if(array_key_exists("description",$data)){{$data["description"]}}@endif">@if(array_key_exists("description",$data)){{$data["description"]}}@endif</textarea>
                        </td>
                      </tr>

                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Connect Image url
                        </td>
                        <td style="border: 1px solid black">
                        <input type="file" name="profileImage" id="profileImage" onchange="loadFile(event)">
                        </td>
                      </tr>

                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Profession
                        </td>
                        <td style="border: 1px solid black">
                            <input style="width: 100%;border:none" type="text" name="professionName" id="professionName" value="@if(array_key_exists("professionName",$data)){{$data["professionName"]}}@endif">
                        </td>
                        <input type="hidden" name="file_path" id="file_path">
                      </tr>
                      <tr>
                        <td style="border: 1px solid black" colspan="2">
                          <button type="submit" class="btn btn-primary" id="update__button">Update</button>
                       </td>
                        
                      </tr>
                    </form>
                </tbody>
            
              </table>

          </div>
          <!-- /.card-body -->
        </div>
      </div>


      <!-- Modal -->

    
@endsection

@push('page_scripts')
<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase.js">     </script>
<script>
    var firebaseConfig = {
      apiKey: "AIzaSyBjqw-6RXYPAwPpoB__advCo3Cr9O_jIhk",
      authDomain: "realestate-6126a.firebaseapp.com",
      databaseURL: "https://realestate-6126a.firebaseio.com",
      projectId: "realestate-6126a",
      storageBucket: "realestate-6126a.appspot.com",
      messagingSenderId: "938815841146",
      appId: "1:938815841146:web:db28f9c286de0f388fecc3",
      measurementId: "G-CDJDMYZKS9"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    function loadFile(event){
      const update__button=document.getElementById('update__button');
      const file_path=document.getElementById('file_path');
      update__button.disabled = true;
      var reader = new FileReader();
      reader.onload = function(){
        var output = document.getElementById('output');
        output.src = reader.result;
      };
      let image=event.target.files[0];
      const ref = firebase.storage().ref('profile');
      const file = event.target.files[0];
      const name = +new Date() + "-" + file.name;
      const metadata = {
        contentType: file.type
      };
      const task = ref.child(name).put(file, metadata);
      task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then(url => {
          file_path.value=url;
          update__button.disabled = false;
        })
        .catch(console.error);
    }
    const approvedbutton=document.querySelector('.btn.btn-success.ml-3');
    const disabledbutton=document.querySelector('.btn.btn-danger.ml-3');
    console.log(approvedbutton,disabledbutton);
    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    function handleaction(id){
      const statusdocument=document.getElementById(`user-${id}`);
     loadercontainer.classList.add('active');
      $.get( `/updateconnect/${id}`, function( data ) {
        if(statusdocument.innerText=="Disable"){
            statusdocument.innerText="Enable";
            approvedbutton.classList.add('d-none');
            disabledbutton.classList.remove('d-none');
          }
          else{
            statusdocument.innerText="Disable";
            
            disabledbutton.classList.add('d-none');
            approvedbutton.classList.remove('d-none');
           
          }
      console.log(data);
      loadercontainer.classList.remove('active');
});
    }
    
    
    var date = new Date((1525227766000));//data[k].timestamp
    console.log(date);
</script>

@endpush