@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass {{$classname}}"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass"><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass"><span>Connects</span></a>
          <a href="{{route('comments')}}" class="buttonClass"><span>Comments</span></a>
         </div>
         <div class="search__container">
            <div>
             <form action="{{route('searchproperties')}}" method="post">
               @csrf
              <input type="text" name="searchtext" placeholder="Search">
              <select name="searchField" id="cars">
                <option value="volvo">Select</option>
                <option value="email">Email</option>
                <option value="address">Address</option>
                <option value="date_added">Date Added</option>
                <option value="property_status">Ownership Type</option>
                <option value="property_visible_status">Property Visible Status</option>
                <option value="date_last_updated">Date last update</option>
                <option value="status">Status</option>
              </select>
              <button type="submit" class="search__button">Search</button>
             </form>
            </div>
  
            <div class="export__element">
              @if ($url==="properties")
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url(1)}}"><<</a>
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->lastPage())}}">>></a>
                @endif
                <form action="{{route('propertiesExport')}}" method="post">
                  @csrf
                 <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                 <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                 <button class="search__button export_button" type="submit">Export</button>
               </form>
            </div>
             
           </div>
 
         <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Likes</th>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>Ownership type</th>
                  <th>Complete Data</th>
                  <th>Property Visible Status</th>
                  <th>Date last update</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                <tr>
                  <td>
                      @php
                        if(array_key_exists("email",$item)){
                          echo $item['email'];
                        }
                      @endphp
                    </td>
                  <td>
                    @php
                      if(array_key_exists("address",$item)){
                        echo $item['address'];
                      }
                    @endphp
                   </td>
                  
                  <td>
                      @php
                      if(array_key_exists("likes",$item)){
                        echo $item["likes"];
                      }
                      @endphp
                  </td>
                  <td>
                   {{-- <script>
                    var adddate={{$item['purchase_date']}}
                    var date = new Date((adddate));//data[k].timestamp
                    document.write(date.toLocaleDateString());
                   </script> --}}
                  @php
                      if(array_key_exists("date_added",$item)){
                        echo $item["date_added"];
                      }
                  @endphp    
                </td>
                  <td id="property-{{$item['Property_id']}}">@php
                      if (array_key_exists("deleted",$item)){
                        foreach ($item as $key => $value) {
                             if($key==="deleted"){
                                 if($item[$key]){
                                  echo "Deleted";
                                 } 
                                 else {
                                  echo "Active";
                                 }
                             }
                      }
                      }
                      else {
                          echo 'Active';
                      }
                  @endphp</td>
                  <td>
                    @php
                      if(array_key_exists("property_status",$item)){
                        echo $item["property_status"];
                      }
                      
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("property_complete_status",$item)){
                        if($item["property_complete_status"]){
                           echo "Complete";
                        }else {
                          echo "Incomplete";
                        }
                      }
                      else {
                        echo "Incomplete";
                      }
                    @endphp
                  </td>
                  <td id="propertyvisiblestatus-{{$item['Property_id']}}">
                    @php
                      if(array_key_exists("property_visible_status",$item)){
                        echo $item["property_visible_status"];
                      }
                      else {
                         echo "Pending";
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                    if(array_key_exists("date_last_updated",$item)){
                      echo $item["date_last_updated"];
                    }
                    @endphp
                  </td>
                  <td style="text-align: center">
                    <ul style="list-style: none;">
                      <li>
                        <button class="btn btn-link" id="approveaction-{{$item['Property_id']}}" onclick="approveaction('{{$item['Property_id']}}')">
                          @php
                          if(array_key_exists("property_visible_status",$item)){
                              if ($item["property_visible_status"]=="forsale") {
                                echo "Approved";
                              }
                              else {
                                echo "Approve";
                              }
                          }
                          else {
                             echo "Approve";
                          }
                        @endphp
                           
                        </button>
                      </li>
                      <li><button class="btn btn-link" id="propertybutton-{{$item['Property_id']}}" onclick="actionbutton('{{$item['Property_id']}}')">
                        @php
                          if (array_key_exists("deleted",$item)){
                            foreach ($item as $key => $value) {
                                 if($key==="deleted"){
                                     if($item[$key]){
                                        echo 'Reverse';      
                                     } 
                                     else {
                                      echo "Delete";
                                     }
                                 }
                          }
                          }
                          else {
                              echo 'Delete';
                          }
                      @endphp
                      </button></li>
                     <li>
                      <a href="{{route('propertiedetail',$item['Property_id'])}}" class="btn btn-link">
                        View Detail
                      </a>
                    </li>
                   
                    </ul>
                  </td>
                </tr>
                @endforeach
             
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
          </div>
    </div>
@endsection

@push('page_scripts')
<script>

    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    const approveaction=(id)=>{
      let approveaction=document.getElementById(`approveaction-${id}`);
      let property_visible_status=document.getElementById(`propertyvisiblestatus-${id}`)
      loadercontainer.classList.add('active');
      const xhr=new XMLHttpRequest();
      const url=`/propertystatus/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
        const myresponse=JSON.parse(this.responseText);
          console.log(myresponse);
          if(myresponse.status=="success"){
            property_visible_status.innerText="forsale";
             approveaction.innerText="Approved";
           
              loadercontainer.classList.remove('active');
            }
          }
      xhr.send();
    }
    const actionbutton=(id)=>{
      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`property-${id}`);
      const statusButton=document.getElementById(`propertybutton-${id}`);
      
      const xhr=new XMLHttpRequest();
      const url=`/updateproperties/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
        const myresponse=JSON.parse(this.responseText);
          if(myresponse.status=="success"){
             if(statusdocument.innerText=="Deleted"){
                 statusdocument.innerText="Active";
                 statusButton.innerText="Delete";
             }
              else{
                 statusdocument.innerText="Deleted";
                 statusButton.innerText="Reverse";
              }
              loadercontainer.classList.remove('active');
            }
          }
      xhr.send();
    };
</script>

@endpush