@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass {{$classname}}"><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass"><span>Connects</span></a>
          <a href="{{route('comments')}}" class="buttonClass"><span>Comments</span></a>
         </div>
         <div class="search__container">
            <div>
             <form action="{{route('searchimage')}}" method="post">
               @csrf
              <input type="text" name="searchtext" placeholder="Search">
              <select name="searchField" id="cars">
                <option value="volvo">Select</option>
                <option value="email">Email</option>
                <option value="location">Location</option>
                <option value="address">Property Address/ Username</option>
                <option value="latest_added_date">Date Added</option>

              </select>
              <button type="submit" class="search__button">Search</button>
             </form>
            </div>
  
            <div class="export__element">
              @if ($url==="images")
              <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url(1)}}"><<</a>
              <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
              <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
              <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->lastPage())}}">>></a>
                @endif
                <form action="{{route('imagesExport')}}" method="post">
                  @csrf
                 <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                 <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                 <button class="search__button export_button" type="submit">Export</button>
               </form>
            </div>
             
           </div>
 
         <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>Email</th>
                  {{-- <th>Image Id</th> --}}
                  <th>Location</th>
                  <th>Property Address/ Username</th>
                  <th>Image</th>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                @if (array_key_exists("image",$item))
                <tr>
                  <td>
                    @php
                    if(array_key_exists("email",$item)){
                       echo $item["email"];
                    }
                     @endphp
                    </td>
                  {{-- <td>
                    @if (array_key_exists("image_index",$item))
                        @if (array_key_exists("image",$item))
                            @if (strlen($item["image"])>0)
                            <a href="{{$item["image"]}}">{{$item["image_index"]}}</a>  
                            @else
                            <p>{{$item["image_index"]}}</p>
                            @endif    
                        @else
                            <p>{{$item["image_index"]}}</p>
                        @endif
                    @endif
                    </td> --}}
                  <td>
                    @php
                    if(array_key_exists("location",$item)){
                       echo $item["location"];
                    }
                     @endphp
                  </td>
                  <td>
                    @php
                    if(array_key_exists("address",$item)){
                       echo $item["address"];
                    }
                     @endphp
                  </td>
                  
                  <td>
                    @if (array_key_exists("image",$item))
                        @if (strlen($item["image"])>0)
                        <a href="{{$item["image"]}}" data-lightbox="mygallery" data-title="image_title"> <img src="{{$item["image"]}}" alt="Mountains" style="height:80px;width:80px"></a>    
                          {{-- <img src="{{$item["image"]}}" style="width: 60px;height:60px;" alt="property image">  --}}
                        @endif
                    @endif
                  </td>
                <td>
                 @if (array_key_exists('latest_added_date',$item))
                 {{$item['latest_added_date']}}
                 {{-- <script>
                   var adddate={{$item['added_date']}}
                   var date = new Date((adddate));//data[k].timestamp
                   document.write(date.toLocaleDateString());
                </script> --}}
                 @endif
                </td>
                  <td>
                    <span id="status-{{$item['properties_id']}}-{{$item['image_index']}}-{{$item['reserve']}}">
                      @if ($item["reserve"]==1)
                        Deleted  
                      @else
                        Active
                      @endif
                
                    </span> 
                    
                  </td>
                  <td>
                  <div id="imagebutton-{{$item['properties_id']}}">
                    <button class="btn btn-link"id="actionbutton-{{$item['properties_id']}}-{{$item['image_index']}}-{{$item['reserve']}}" onclick="handleaction('{{$item['properties_id']}}','{{$item['image_index']}}','{{$item['reserve']}}')" >
                          @if ($item["reserve"]==1)
                            Reverse
                          @else
                            Delete
                          @endif
                      </button>
                  </div>
                  </td>
                </tr>        
                @else
                @if (array_key_exists('User_id',$item))
                <tr>
                  <td>
                    @php
                    if(array_key_exists("email",$item)){
                       echo $item["email"];
                    }
                     @endphp
                    </td>
                  {{-- <td>
                    @if (array_key_exists("image_index",$item))
                        @if (array_key_exists("image",$item))
                            @if (strlen($item["image"])>0)
                            <a href="{{$item["image"]}}">{{$item["image_index"]}}</a>  
                            @else
                            <p>{{$item["image_index"]}}</p>
                            @endif    
                        @else
                            <p>{{$item["image_index"]}}</p>
                        @endif
                    @endif
                    </td> --}}
                  <td>
                    @php
                    if(array_key_exists("location",$item)){
                       echo $item["location"];
                    }
                     @endphp
                  </td>
                  <td> @php
                    if(array_key_exists("username",$item)){
                       echo $item["username"];
                    }
                     @endphp</td>
                  <td>
                    @if (array_key_exists("profileImage",$item))
                    <a href="{{$item["profileImage"]}}" data-lightbox="mygallery" data-title="image_title"> <img src="{{$item["profileImage"]}}" alt="Mountains" style="height:80px;width:80px"></a>    
                    @endif
                  </td>
                <td>
                 @if (array_key_exists('latest_added_date',$item))
                      {{$item['latest_added_date']}}
                 {{-- <script>
                  var adddate={{$item['added_date']}}
                  var date = new Date((adddate));//data[k].timestamp
                  document.write(date.toLocaleDateString());
                </script>        
                @else
                     @php
                          $start = strtotime("01 January 2021");
                          $end = strtotime("31 December 2021");
                          $timestamp = mt_rand($start, $end);
                          $created_date=date("m/d/Y", $timestamp);
                          echo $created_date;
                     @endphp --}}
                 @endif
                </td>
                <td id="user-{{$item['User_id']}}">@php
                  if (array_key_exists("notif",$item)){
                    foreach ($item as $key => $value) {
                         if($key==="notif"){
                             if($item[$key]){
                              echo "Deleted";
                             } 
                             else {
                              echo "Active";
                             }
                         }
                  }
                  }
                  else {
                      echo 'Active';
                  }
                  
              @endphp</td>
                  <td>
                    <button class="btn btn-link" id="userbutton-{{$item['User_id']}}" onclick="handleanotheraction('{{$item['User_id']}}')">
                      @php
                        if (array_key_exists("notif",$item)){
                          foreach ($item as $key => $value) {
                               if($key==="notif"){
                                   if($item[$key]){
                                      echo 'Reverse'; 
                                   } 
                                   else {
                                      echo "Delete";
                                   }
                               }
                        }
                        }
                        else {
                            echo 'Delete';
                        }
                    @endphp
                    </button>
                  </td>
                </tr>  
                @endif
             
                @endif
                @endforeach
             
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        

      

      </div>
    
@endsection

@push('page_scripts')
<script>
    const loadercontainer=document.getElementById('loader_container');
    const addClass=document.querySelectorAll('.buttonClass')
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    const handleanotheraction=(id)=>{
      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`user-${id}`);
      const statusButton=document.getElementById(`userbutton-${id}`);
      
      const xhr=new XMLHttpRequest();
      const url=`/updateuser/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
          const myresponse=JSON.parse(this.responseText);
          if(myresponse.status=="success"){
            if(statusdocument.innerText=="Deleted"){
            statusdocument.innerText="Active";
            statusButton.innerText="Delete";
          }
          else{
            statusdocument.innerText="Deleted";
            statusButton.innerText="Reverse";
          }
          loadercontainer.classList.remove('active');
        }
      }
      xhr.send(); 
    }
    const handleaction=(id,imgindex,reverse)=>{
      loadercontainer.classList.add('active');
      console.log(id,imgindex,reverse);
      const actionButton=document.getElementById(`actionbutton-${id}-${imgindex}-${reverse}`);
      const statusaction=document.getElementById(`status-${id}-${imgindex}-${reverse}`);
      console.log(actionButton.innerText,statusaction.innerText)
      if(actionButton.innerText=="Delete"){
         const xhr=new XMLHttpRequest();
         const url=`/deleteimage/${id}?image=${imgindex}`;
         xhr.open('GET',url,true);
         xhr.onload=function(){
         const response=JSON.parse(this.responseText);
         if(response.status==="success"){
          actionButton.innerText="Reverse";
            statusaction.innerText="Deleted";
            loadercontainer.classList.remove('active');
          }
         }
         xhr.send(); 

        
      }
      else{
        const xhr=new XMLHttpRequest();
        const url=`/reverseImage/${id}?image=${imgindex}`;
        xhr.open('GET',url,true);
        xhr.onload=function(){
          const response=JSON.parse(this.responseText);
          if(response.status=="success"){
          actionButton.innerText="Delete";
            statusaction.innerText="Active";
            loadercontainer.classList.remove('active');
          }
         }
        xhr.send();
        
      }
     
    };

  

</script>

@endpush