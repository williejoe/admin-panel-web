@extends('admin.dashboard.layouts.app')
@php
    $array = array("1" => 'Realtor', "2" => 'Wholesaler', "3" => 'Property Manager', "4" => 'Contractor',"5"=>"Lender");
@endphp
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass "><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass {{$classname}}"><span>Connects</span></a>
          <a href="{{route('comments')}}" class="buttonClass"><span>Comments</span></a>
         </div>
         <div class="search__container">
            <div style="display: flex">
             <form action="{{route('searchconnect')}}" method="post">
               @csrf
              <input type="text" name="searchtext" placeholder="Search" value="{{$sessiontext}}">
              <select name="searchField" id="cars">
                <option value="all" @if ($sessiondrop=="all") {{"selected"}} @endif>All</option>                
                <option value="name" @if ($sessiondrop=="name") {{"selected"}} @endif>Name</option>
                <option value="email" @if ($sessiondrop=="email") {{"selected"}} @endif>Email</option>
                <option value="phone" @if ($sessiondrop=="phone") {{"selected"}} @endif>Phone</option>
                <option value="professionName" @if ($sessiondrop=="professionName") {{"selected"}} @endif>Profession</option>
                <option value="date_create" @if ($sessiondrop=="date_create") {{"selected"}} @endif>Date Added</option>
                <option value="date_Updated" @if ($sessiondrop=="date_Updated") {{"selected"}} @endif>Date last update</option>

                {{-- <option value="firstname">Firstname</option> --}}
              </select>
              <button type="submit" class="search__button">Search</button>
             </form>
            <div style="display: flex">
              <label for="status" style="margin-right:15px" class="search__button">status</label>
              <form action="{{route('searchimage')}}" method="post">
                @csrf
               <select name="status" id="status" onchange="showstatusdata(this.value)">
               <option value="select" selected>All</option>
                 <option value="2">Approved</option>
                 <option value="1">Disabled</option>
               </select>
              </form>
            </div>
            </div>
            {{-- <div style="display: flex;margin-left:-200px;margin-top:6px">
              <label for="status" style="margin-right:15px" class="search__button">status</label>
              <form action="{{route('searchimage')}}" method="post">
                @csrf
               <select name="status" id="status">
               <option value="volvo">Select</option>
                 <option value="approved">Approved</option>
                 <option value="disabled">Disabled</option>
               </select>
              </form>
             </div> --}}
            
            <div class="export__element">
              @if ($url==="connects")
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url(1)}}"><<</a>
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->lastPage())}}">>></a>
                @endif
                <form action="{{route('connectsExport')}}" method="post">
                  @csrf
                 <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                 <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                 <button class="search__button export_button" type="submit">Export</button>
               </form>

               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Import
              </button>
            </div>
             
           </div>
           @if(session()->has('status'))
            <div style="width: 50%;margin:auto">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Message!</h5>
                Successfully added record
              </div>
          </div>
           @endif
         
         <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>Connect Name</th>
                  <th>Connect Email</th>
                  <th>Connect Phone</th>
                  <th>Connect Profession</th>
                  <th>Date Added</th>
                  <th>Status</th>
                  <th>Date last updated</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="statustablebody">
                @foreach ($data as $Pkey=> $item)
              @if (array_key_exists("status",$item))
          
                @php
                $connectid='';
                if(array_key_exists("U_id",$item)){
                  $connectid=$item["U_id"];
                }
                else {
                  $connectid=$Pkey;
                }
            @endphp
                <tr>
                  <td>
                    @php
                        if(array_key_exists("name",$item)){
                           echo $item["name"];
                        }
                    @endphp
                  </td>
                  <td>
                    @php
                    if(array_key_exists("email",$item)){
                       echo $item["email"];
                    }
                     @endphp
                    </td>
                  <td>
                  @php
                    if(array_key_exists("phone",$item)){
                       echo $item["phone"];
                    }
                     @endphp
                    
                    </td>
                  <td>
                  @php
                    if(array_key_exists("professionId",$item)){
                       if(array_key_exists($item["professionId"],$array)){
                          echo $array[$item["professionId"]];
                       } 
                    }
                     @endphp
                  </td>
                  
                  <td>
                    {{-- <script>
                      var adddate={{$item['date_created']}};
                      var date = new Date((adddate));//data[k].timestamp
                      document.write("date");

                     </script> --}}
                     @php
                     if(array_key_exists("date_create",$item)){
                          $date = date_create($item["date_create"]);
                          echo date_format($date,'Y-m-d');
                     }
                     else {
                         echo date('Y-m-d');
                     }
                      @endphp
                  </td>
                  <td id="user-{{$connectid}}">
                    @php
                    if(array_key_exists("status",$item)){
                           if ($item["status"]=="2") {
                            echo "Approved";
                           } else {
                            echo "Disabled";
                           }
                    }
                     @endphp
                </td>
                
                <td>
                  {{-- <script>
                    var adddate={{$item['date_last_updated']}}
                    var date = new Date((adddate));//data[k].timestamp
                    document.write(date.toLocaleDateString());
                   </script> --}}
                     @php
                     if(array_key_exists("date_Updated",$item)){
                           $date = date_create($item["date_Updated"]);
                          echo date_format($date,'Y-m-d');
                     }
                     else {
                         echo date('Y-m-d');
                     }
                     
                      @endphp
                </td>
             
                  <td>
                    <ul style="list-style: none;">
                      <li> <button class="btn btn-link" id="userbutton-{{$connectid}}" onclick="handleaction('{{$connectid}}')">
                        @php
                             if(strtolower($item["status"])=="2"){
                                echo 'Disabled'; 
                             } 
                             else {
                                  echo "Approved";
                             }
                        @endphp
                      </button></li>
                      <li> <a href="{{route('detail',$connectid)}}">View Detail</a> </li>
                    </ul>
                  </td>
                </tr>
                @endif
                @endforeach
             
               
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        

      

      </div>


      <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Import Connects</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="connect_importing" method="POST" action="{{route('connectsImport')}}" enctype="multipart/form-data">
                  @csrf
                 <input type="file" name="importfile" required="required">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Import</button>
        </form>
      </div>
    </div>
  </div>
</div>
    
@endsection

@push('page_scripts')
<script>
    const statustablebody=document.getElementById('statustablebody');
    function showstatusdata(status){
      if(status=="select"){
            window.location.href='/connects';
            return;
      }
      loadercontainer.classList.add('active');
      $.get( `/connects/${status}`, function( data ) {
        
        if(data.status=="success"){
          let tableinner='';
          if(status=="select"){
           return;
          }
          else{
          const professions = ["Realtor", "Wholesaler", "Property Manager","Contractor","Lender"];
          data.data.forEach((element,index) => {
            let id='';
            let status='';
            let showstatus='';
            if (element.status=="2") {
              status="Disabled";
              showstatus="Approved";
            } else {
              status="Approved";
              showstatus="Disabled";
            }
            if (element.U_id) {
              id=element.U_id;
            } else {
              id=index;
            }
            let professionid=parseInt(element.professionId)-1;
            const profession=professions[professionid];
            var date = new Date();//data[k].timestamp
            var date1 = new Date();//data[k].timestamp
            tableinner+=`<tr><td>${id}</td>
                            <td>${element.name}</td>
                            <td>${element.email}</td>
                            <td>${element.phone}</td>
                            <td>${profession}</td>
                            <td>${date.toLocaleDateString()}</td>
                            <td id="user-${id}">${showstatus}</td>
                           <td>${date1.toLocaleDateString()}</td>
                           <td>
                              <ul style="list-style: none;">
                                <li> <button class="btn btn-link" id="userbutton-${id}" onclick="handleaction('${id}')">
                                  ${status} </button></li>
                                <li> <a href="/detail/${id}">View Detail</a> </li>
                              </ul>
                            </td></tr>`;
          });
          }
          
    
          statustablebody.innerHTML=tableinner;
        }
      loadercontainer.classList.remove('active');
});
      // if(status=="select"){
      //   window.location.href=`/connects`;
      // }
      // else{
      //   window.location.href=`/connects/${status}`;
      // }
   
    }
    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }

    const handleaction=(id)=>{

      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`user-${id}`);
      const statusButton=document.getElementById(`userbutton-${id}`);
      
      const xhr=new XMLHttpRequest();
      const url=`/updateconnect/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
          const myresponse=JSON.parse(this.responseText);
          console.log(myresponse);
          if(myresponse.status=="success"){
          if(statusButton.innerText=="Disabled"){
            statusdocument.innerText="Disabled";
            statusButton.innerText="Approved";
          }
          else{
            statusdocument.innerText="Approved";
            statusButton.innerText="Disabled";
          }
          loadercontainer.classList.remove('active');
        }
      }
      xhr.send(); 
    }
    
    var date = new Date((1525227766000));//data[k].timestamp
    console.log(date);
</script>

@endpush