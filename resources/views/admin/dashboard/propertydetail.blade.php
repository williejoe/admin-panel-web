@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
        <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass {{$classname}}"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass "><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass"><span>Connects</span></a>
          <a href="{{route('connects')}}" class="buttonClass"><span>Comments</span></a>
         </div>
 
         <div>
       
          <!-- /.card-header -->
          <div>
              <div style="display: flex;margin-bottom:20px;align-items:center">
                <a href="{{route("properties")}}" class="btn btn-secondary mr-3">Back</a>
                <img id="property__image" src="@php
                      if(array_key_exists("property_image",$data)){
                           echo $data["property_image"];
                     }
                     else {
                         echo "";
                     }
                @endphp" alt="" style="width: 200px;height:200px:border-radius:10px">
                      <button class="btn btn-success ml-3 @if (array_key_exists('deleted',$data))
                      @if (!$data['deleted'])
                        {{"d-none"}}
                      @endif @endif" onclick="handleaction('{{$id}}')">Enabled</button>
                      <button class="btn btn-danger ml-3 @if (array_key_exists('deleted',$data))
                      @if ($data['deleted'])
                        {{"d-none"}}
                      @endif @endif" onclick="handleaction('{{$id}}')">Disabled</button>
              </div>
            @if(session()->has('status'))
              <div style="width: 50%;margin:auto">
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> Message!</h5>
                  Successfully updated
                </div>
            </div>
             @endif
            <table style="width: 70%">
              <tbody>
                <form action="{{route('updateproperties',$id)}}" method="post" enctype="multipart/form-data">
                  @csrf
                <tr style="height: 30px">
                  <td style="border: 1px solid black">
                   Image Upload 
                  </td>
                  <td style="border: 1px solid black">
                    <input type="file" name="profileImage" id="profileImage" onchange="loadFile(event)">
                  </td>
                </tr>
                <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Description
                    </td>
                    <td style="border: 1px solid black">
                      <textarea style="width: 100%;border:none" rows="4" type="text" name="property_desc" id="property_desc" >@if(array_key_exists("property_desc",$data)){{$data["property_desc"]}}@endif</textarea>
                    </td>
                  </tr>
              </tbody>
            </table>
         
            <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Email
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="email" name="email" id="" value="@if(array_key_exists("email",$data)){{$data["email"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Address 
                    </td>
                    <td style="border: 1px solid black">
                    <input style="border: none;width:100%" type="text" name="address" id="" value="@if(array_key_exists("address",$data)){{$data["address"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                       Date Added
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="date" name="date_added" id="date_added" value="@if(array_key_exists("date_added",$data)){{$data["date_added"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Status
                      </td>
                      <td style="border: 1px solid black" id="show_property_status">
                        @php
                        if (array_key_exists("deleted",$data)){
                          foreach ($data as $key => $value) {
                               if($key==="deleted"){
                                   if($data[$key]){
                                    echo "Deleted";
                                   } 
                                   else {
                                    echo "Active";
                                   }
                               }
                        }
                        }
                        else {
                            echo 'Active';
                        }
                    @endphp
                      </td>
                    </tr>
                    <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Ownership Status
                        </td>
                        <td style="border: 1px solid black">
                         <input style="border: none;width:100%" type="text" name="property_status" id="property_status" value="@if(array_key_exists("property_status",$data)){{$data["property_status"]}}@endif">
                        </td>
                      </tr>
                     
                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                         Complete Data
                        </td>
                        <td style="border: 1px solid black">
                          <input style="border: none;width:100%" type="text" name="property_complete_status" id="property_complete_status" value="@if(array_key_exists("property_complete_status",$data))@if ($data["property_complete_status"]){{"Complete"}}@else{{"Incomplete"}}@endif @endif">
                        </td>
                      </tr>
                      <tr style="height: 30px">
                        <td style="border: 1px solid black">
                          Date Last Update
                        </td>
                        <td style="border: 1px solid black">
                          <input style="border: none;width:100%" type="date" name="date_last_updated" id="date_last_updated" value="@if(array_key_exists("date_last_updated",$data)){{$data["date_last_updated"]}}@endif">
                        </td>
                      </tr>
                </tbody>
              </table>
              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Market Value
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="market_value" id="" value="@if(array_key_exists("market_value",$data["units"][0])){{$data["units"][0]["market_value"]}}@endif">
                    </td>
                  </tr>
                </tbody>
              </table>

              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Property Type
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="prop_type" id="" value="@if(array_key_exists("prop_type",$data)){{$data["prop_type"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Bedroom 
                    </td>
                    <td style="border: 1px solid black">
                    <input style="border: none;width:100%" type="text" name="bedrooms" value="@if(array_key_exists("bedrooms",$data["units"][0])){{$data["units"][0]["bedrooms"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Bathroom
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="bathrooms" value="@if(array_key_exists("bathrooms",$data["units"][0])){{$data["units"][0]["bathrooms"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Square Feet
                      </td>
                      <td style="border: 1px solid black">
                      <input style="border:none;width:100%" type="text" name="square_feet" id="square_feet" value="@if(array_key_exists("square_feet",$data["units"][0])){{$data["units"][0]["square_feet"]}}@endif">
                      </td>
                    </tr>
                </tbody>
              </table>

              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                      Purchase Price
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="purchase_amt" id="" value="@if(array_key_exists("purchase_amt",$data)){{$data["purchase_amt"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                      Purchase Date 
                    </td>
                    <td style="border: 1px solid black">
                    <input style="border: none;width:100%" type="date" name="purchase_date" id="show_purchase_date">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Down Payment
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="down_payment" id="down_payment" value="@if(array_key_exists("down_payment",$data)){{$data["down_payment"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Closing Cost
                      </td>
                      <td style="border: 1px solid black">
                      <input style="border:none;width:100%" type="text" name="closing_cost" id="closing_cost" value="@if(array_key_exists("closing_cost",$data)){{$data["closing_cost"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Initial Rehab Cost
                      </td>
                      <td style="border: 1px solid black">
                      <input style="border:none;width:100%" type="text" name="rehab_cost" id="rehab_cost" value="@if(array_key_exists("rehab_cost",$data)){{$data["rehab_cost"]}}@endif">
                      </td>
                    </tr>
                </tbody>
              </table>

              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                      Rent
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="rent_month" placeholder="(Month)" id="" value="@if(array_key_exists("rent_month",$data["units"][0])){{$data["units"][0]["rent_month"]}}@endif">
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="rent_annual" placeholder="(Year)" id="" value="@if(array_key_exists("rent_annual",$data["units"][0])){{$data["units"][0]["rent_annual"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                      Rent Payment Date 
                    </td>
                    <td style="border: 1px solid black" colspan="2">
                    <input style="border: none;width:100%" type="date" name="rent_start" id="rent_start" value="@if(array_key_exists("rent_start",$data)){{$data["rent_start"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                      Lease Start Date
                    </td>
                    <td style="border: 1px solid black" colspan="2">
                    <input style="border: none;width:100%" type="date" name="rent_end" id="rent_end" value="@if(array_key_exists("rent_end",$data)){{$data["rent_end"]}}@endif">
                    </td>
                  </tr>
                
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Rent Term
                      </td>
                      <td style="border: 1px solid black" colspan="2">
                        <input style="border:none;width:100%" type="text" name="rent_term" id="rent_term" value="@if(array_key_exists("rent_term",$data)){{$data["rent_term"]}}@endif">
                      </td>
                    </tr>
                </tbody>
              </table>

              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Insurance
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="month_ins" placeholder="(Month)" id="" value="@if(array_key_exists("month_ins",$data["units"][0])){{$data["units"][0]["month_ins"]}}@endif">
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="annual_ins" placeholder="(Year)" id="" value="@if(array_key_exists("annual_ins",$data["units"][0])){{$data["units"][0]["annual_ins"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Property Tax 
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="month_prot" placeholder="(Month)" id="" value="@if(array_key_exists("month_prot",$data["units"][0])){{$data["units"][0]["month_prot"]}}@endif">
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="annual_prot" placeholder="(Year)" id="" value="@if(array_key_exists("annual_prot",$data["units"][0])){{$data["units"][0]["annual_prot"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Vacancy Allowance
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_vac" placeholder="(Month)" id="" value="@if(array_key_exists("month_vac",$data["units"][0])){{$data["units"][0]["month_vac"]}}@endif">
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="annual_vac" placeholder="(Year)" id="" value="@if(array_key_exists("annual_vac",$data["units"][0])){{$data["units"][0]["annual_vac"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Repair Allowance
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_repair" placeholder="(Month)" id="" value="@if(array_key_exists("month_repair",$data["units"][0])){{$data["units"][0]["month_repair"]}}@endif">
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="annual_repair" placeholder="(Year)" id="" value="@if(array_key_exists("annual_repair",$data["units"][0])){{$data["units"][0]["annual_repair"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Property Management
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_prom" placeholder="(Month)" id="" value="@if(array_key_exists("month_prom",$data["units"][0])){{$data["units"][0]["month_prom"]}}@endif">
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="annual_prom" placeholder="(Year)" id="" value="@if(array_key_exists("annual_prom",$data["units"][0])){{$data["units"][0]["annual_prom"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Utilities
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_util" placeholder="(Month)" id="" value="@if(array_key_exists("month_util",$data["units"][0])){{$data["units"][0]["month_util"]}}@endif">
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="annual_util" placeholder="(Year)" id="" value="@if(array_key_exists("annual_util",$data["units"][0])){{$data["units"][0]["annual_util"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Homeowner's Association
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_hoa" placeholder="(Month)" id="" value="@if(array_key_exists("month_hoa",$data["units"][0])){{$data["units"][0]["month_hoa"]}}@endif">
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="annual_hoa" placeholder="(Year)" id="" value="@if(array_key_exists("annual_hoa",$data["units"][0])){{$data["units"][0]["annual_hoa"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                         Other
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_other" placeholder="(Year)" id="" value="@if(array_key_exists("month_other",$data["units"][0])){{$data["units"][0]["month_other"]}}@endif">
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="annual_other" placeholder="(Year)" id="" value="@if(array_key_exists("annual_other",$data["units"][0])){{$data["units"][0]["annual_other"]}}@endif">
                      </td>
                    </tr>
                </tbody>
              </table>
              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Capital Expenditures
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="capex_month" id="" placeholder="(Month)" value="@if(array_key_exists("capex_month",$data["units"][0])){{$data["units"][0]["capex_month"]}}@endif">
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="capex_annual" placeholder="(Year)" id="" value="@if(array_key_exists("capex_annual",$data["units"][0])){{$data["units"][0]["capex_annual"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Depreciation 
                    </td>
                    <td style="border: 1px solid black">
                    <input style="border: none;width:100%" type="text" name="depreciation_month" id="" value="@if(array_key_exists("depreciation_month",$data["units"][0])){{$data["units"][0]["depreciation_month"]}}@endif">
                    </td>
                    <td style="border: 1px solid black">
                      <input style="border: none;width:100%" type="text" name="depreciation_year" placeholder="(Year)" id="depreciation_year" value="@if(array_key_exists("depreciation_year",$data["units"][0])){{$data["units"][0]["depreciation_year"]}}@endif">
                    </td>
                  </tr>
                  <input type="hidden" name="file_path" id="file_path">
                  <input type="hidden" name="hidden_purchase_date" id="hidden_purchase_date" value="@if(array_key_exists("purchase_date",$data)){{$data["purchase_date"]}}@endif">
                  <input type="hidden" name="hidden_rent_payment_date" id="hidden_rent_payment_date" value="@if(array_key_exists("rent_start",$data["units"][0])){{$data["units"][0]["rent_start"]}}@endif">
                  <input type="hidden" name="hidden_lease_start_date" id="hidden_lease_start_date" value="@if(array_key_exists("rent_end",$data["units"][0])){{$data["units"][0]["rent_end"]}}@endif">
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Other
                      </td>
                      <td style="border: 1px solid black">
                        <input style="border: none;width:100%" type="text" name="month_other" placeholder="(Month)" id="" value="@if(array_key_exists("month_other",$data["units"][0])){{$data["units"][0]["month_other"]}}@endif">
                        </td>
                        <td style="border: 1px solid black">
                          <input style="border: none;width:100%" type="text" name="annual_other" placeholder="(Year)" id="annual_other" value="@if(array_key_exists("annual_other",$data["units"][0])){{$data["units"][0]["annual_other"]}}@endif">
                        </td>
                    </tr>
                </tbody>
              </table>
              <table style="width: 70%;margin-top:15px">
                <tbody>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                      Purchase Price
                    </td>
                    <td style="border: 1px solid black" colspan="2">
                      <input style="border: none;width:100%" type="text" name="purchase_amt" id="" value="@if(array_key_exists("purchase_amt",$data)){{$data["purchase_amt"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                    <td style="border: 1px solid black">
                     Down Payment 
                    </td>
                    <td style="border: 1px solid black" colspan="2">
                      <input style="border: none;width:100%" type="text" name="down_payment" id="down_payment" value="@if(array_key_exists("down_payment",$data)){{$data["down_payment"]}}@endif">
                    </td>
                  </tr>
                  <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Interest Rate
                      </td>
                      <td style="border: 1px solid black" colspan="2">
                        <input style="border: none;width:100%" type="text" name="mtg_interest" placeholder="(Year)" id="" value="@if(array_key_exists("mtg_interest",$data["units"][0])){{$data["units"][0]["mtg_interest"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Loan term
                      </td>
                      <td style="border: 1px solid black" colspan="2">
                      <input style="border:none;width:100%" type="text" name="mtg_loan" id="mtg_loan" value="@if(array_key_exists("mtg_loan",$data["units"][0])){{$data["units"][0]["mtg_loan"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Mortgage Insurance
                      </td>
                      <td style="border: 1px solid black">
                      <input style="border:none;width:100%" type="text" name="mtg_interest_month" id="mtg_interest_month" value="@if(array_key_exists("mtg_interest_month",$data["units"][0])){{$data["units"][0]["mtg_interest_month"]}}@endif">
                      </td>
                       <td style="border: 1px solid black" colspan="2">
                        <input style="border: none;width:100%" type="text" name="mtg_interest_annual" placeholder="(Year)" id="" value="@if(array_key_exists("mtg_interest_annual",$data["units"][0])){{$data["units"][0]["mtg_interest_annual"]}}@endif">
                      </td>
                    </tr>
                    <tr style="height: 30px">
                      <td style="border: 1px solid black">
                        Loan Balance
                      </td>
                      <td style="border: 1px solid black" colspan="2">
                        <input style="border: none;width:100%" type="text" name="loan_balance" placeholder="(Year)" id="" value="@if(array_key_exists("loan_balance",$data["units"][0])){{$data["units"][0]["loan_balance"]}}@endif">
                      </td>
                    </tr>
                    <tr>
                      <td style="border: 1px solid black" colspan="3">
                        <button type="submit" class="btn btn-primary" id="update__button">Update</button>
                     </td>  
                    </tr>
                </tbody>
              </table>
            </form>

          </div>
          <!-- /.card-body -->
        </div>
      </div>


      <!-- Modal -->

    
@endsection

@push('page_scripts')
<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase.js"></script>
<script>
    function generate_date(purchase_date){
      var date = new Date((parseInt(purchase_date)));//data[k].timestamp
      var d = date.getDate();
      var m = date.getMonth() + 1;
      var y = date.getFullYear();
      var dateString =y+'-'+(m <= 9 ? '0' + m : m)+'-'+(d <= 9 ? '0' + d : d);
      return dateString;
    }
    $( document ).ready(function() {
      
      var purchase_date = $('#hidden_purchase_date').val();
      var rentdate_date = $('#hidden_rent_payment_date').val();
      var leasedate_date = $('#hidden_lease_start_date').val();
      
      var purchase_string=generate_date(purchase_date);
      var rent_string=generate_date(rentdate_date);
      var lease_string=generate_date(leasedate_date);

      document.getElementById('show_purchase_date').value=purchase_string;
      document.getElementById('rent_start').value=rent_string;
      document.getElementById('rent_end').value=lease_string;
      console.log("this is date",purchase_string,"2014-02-09");
    });
    var firebaseConfig = {
      apiKey: "AIzaSyBjqw-6RXYPAwPpoB__advCo3Cr9O_jIhk",
      authDomain: "realestate-6126a.firebaseapp.com",
      databaseURL: "https://realestate-6126a.firebaseio.com",
      projectId: "realestate-6126a",
      storageBucket: "realestate-6126a.appspot.com",
      messagingSenderId: "938815841146",
      appId: "1:938815841146:web:db28f9c286de0f388fecc3",
      measurementId: "G-CDJDMYZKS9"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    function loadFile(event){
      const update__button=document.getElementById('update__button');
      const file_path=document.getElementById('file_path');
      const property__image=document.getElementById('property__image');
      update__button.disabled = true;
      var reader = new FileReader();
      reader.onload = function(){
        var output = document.getElementById('output');
        output.src = reader.result;
      };
      let image=event.target.files[0];
      const ref = firebase.storage().ref('latestproperty');
      const file = event.target.files[0];
      const name = +new Date() + "-" + file.name;
      const metadata = {
        contentType: file.type
      };
      const task = ref.child(name).put(file, metadata);
      task
        .then(snapshot => snapshot.ref.getDownloadURL())
        .then(url => {
          file_path.value=url;
          // property__image.url=url;
          update__button.disabled = false;
        })
        .catch(console.error);
    }
    const approvedbutton=document.querySelector('.btn.btn-success.ml-3');
    const disabledbutton=document.querySelector('.btn.btn-danger.ml-3');
    
    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    function handleaction(id){
      // loadercontainer.classList.add('active');

      const statusdocument=document.getElementById(`show_property_status`);
      console.log(statusdocument)
      const xhr=new XMLHttpRequest();
      const url=`/updateproperties/${id}`;
      xhr.open('GET',url,true);
      xhr.onload=function(){
        const myresponse=JSON.parse(this.responseText);
          if(myresponse.status=="success"){
          if(statusdocument.innerText.includes("Deleted")){
            statusdocument.innerText="Active";
            approvedbutton.classList.add('d-none');
            disabledbutton.classList.remove('d-none');
          }
          else{
            statusdocument.innerText="Deleted";
            disabledbutton.classList.add('d-none');
            approvedbutton.classList.remove('d-none');
          }
          // loadercontainer.classList.remove('active');
          }
          }
      xhr.send();
      }
    
    
    // var date = new Date((1525227766000));//data[k].timestamp
    // console.log(date);
</script>

@endpush