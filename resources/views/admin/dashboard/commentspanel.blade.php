@extends('admin.dashboard.layouts.app')
@section('content')
    <div class="container1 table">
         <div class="button__div">
          <a href="{{route('dashboard')}}" class="buttonClass"><span>Account</span></a>
          <a href="{{route('properties')}}" class="buttonClass"><span>Properties</span></a>
          <a href="{{route('images')}}" class="buttonClass"><span>Images</span></a>
          <a href="{{route('connects')}}" class="buttonClass"><span>Connects</span></a>
          <a href="{{route('comments')}}" class="buttonClass {{$classname}}"><span>Comments</span></a>
        
         </div>
         <div class="search__container">
            <div>
             <form action="{{route('searchcomments')}}" method="post">
               @csrf
              <input type="text" name="searchtext" placeholder="Search" value="{{$sessiontext}}">
              <select name="searchField" id="cars">
                <option value="all" @if ($sessiondrop=="all") {{"selected"}} @endif>All</option>
                <option value="current_status" @if ($sessiondrop=="current_status") {{"selected"}} @endif>Property Status</option>
                <option value="complete" @if ($sessiondrop=="complete") {{"selected"}} @endif>Complete Data</option>
                <option value="property_status" @if ($sessiondrop=="property_status") {{"selected"}} @endif>OwnerShip Type</option>
                <option value="address" @if ($sessiondrop=="address") {{"selected"}} @endif>Property Address</option>
                <option value="comment_desc" @if ($sessiondrop=="comment_desc") {{"selected"}} @endif>Comment</option>
                <option value="comment_by_name" @if ($sessiondrop=="comment_by_name") {{"selected"}} @endif>Commenter Name</option>
                <option value="comment_by_email" @if ($sessiondrop=="comment_by_email") {{"selected"}} @endif>Commenter Email</option>
                <option value="comment_date" @if ($sessiondrop=="comment_date") {{"selected"}} @endif>Date Commented</option>
                {{-- <option value="name" @if ($sessiondrop=="name") {{"selected"}} @endif>Name</option> --}}
              </select>
              <button type="submit" class="search__button">Search</button>
             </form>
            </div>
  
            <div class="export__element">
              @if ($url==="properties")
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url(1)}}"><<</a>
                <a class="pagination__element {{ ($data->currentPage() == 1) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()-1)  }}"><</a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->currentPage()+1) }}">></a>
                <a class="pagination__element {{ ($data->currentPage() == $data->lastPage()) ? ' hideelement' : '' }}" href="/{{$url}}{{ $data->url($data->lastPage())}}">>></a>
                @endif
                <form action="{{route('commentExport')}}" method="post">
                  @csrf
                 <input type="hidden" name="text" value="@isset($text){{ $text }}@endisset">
                 <input type="hidden" name="dropdown" value="@isset($dropdown){{ $dropdown }}@endisset">
                 <button class="search__button export_button" type="submit">Export</button>
               </form>
            </div>
             
           </div>
 
         <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>Property Status</th>
                  <th>Complete Data</th>
                  <th>OwnerShip Type</th>
                  <th>Property Address</th>
                  <th>Comment</th>
                  <th>Commenter Name</th>
                  <th>Commenter Email</th>
                  <th>Date Commented</th>
                  <th>Flag Count</th>
                  <th>Comment Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $Pkey=> $item)
                <tr>
                  <td>
                    @php
                      if(array_key_exists("current_status",$item)){
                        echo $item["current_status"];
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("complete",$item)){
                        echo $item["complete"];
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("property_status",$item)){
                        echo $item["property_status"];
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("address",$item)){
                        echo $item['address'];
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("comment_desc",$item)){
                        echo $item["comment_desc"];
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("comment_by_name",$item)){
                      echo $item["comment_by_name"];
                      }
                    @endphp  
                  </td>
                  <td>
                    @php
                      if(array_key_exists("comment_by_email",$item)){
                        echo $item["comment_by_email"];
                      }
                    @endphp 
                  </td>
                  <td>
                    @php
                      if(array_key_exists("comment_date",$item)){
                        echo $item["comment_date"];
                      }
                    @endphp
                  </td>
                  <td>
                    @php
                      if(array_key_exists("Flag",$item)){
                        echo $item["Flag"];
                      }
                    @endphp 
                  </td>
                  <td id="comment-{{$item['comment_id']}}">
                    @php
                      if(array_key_exists("is_deleted",$item)){
                        if ($item["is_deleted"]) {
                           echo "Deleted";
                        } else {
                           echo "Active";
                        }
                      }
                    @endphp
                  </td>
                  <td>
                    <button class="btn btn-link" id="commentbutton-{{$item['comment_id']}}" onclick="handleaction('{{$item['propertie_id']}}','{{$item['comment_id']}}')">
                      @php
                        if(array_key_exists("is_deleted",$item)){
                          if ($item["is_deleted"]) {
                           echo "Reverse";
                          } else {
                            echo "Delete";
                          }
                        }
                      @endphp
                    </button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
          </div>
    </div>
@endsection

@push('page_scripts')
<script>

    const addClass=document.querySelectorAll('.buttonClass');
    const loadercontainer=document.getElementById('loader_container');
    for (const elemnet of addClass) {
        elemnet.addEventListener('click',(e)=>{
            e.target.classList.add('active')
        })
    }
    const handleaction=(pro_id,comment_id)=>{
      loadercontainer.classList.add('active');
      const statusdocument=document.getElementById(`comment-${comment_id}`);
      const statusButton=document.getElementById(`commentbutton-${comment_id}`);
      const xhr=new XMLHttpRequest();
         const url=`/deletecomment/${pro_id}?commentid=${comment_id}`;
         console.log(url);
         xhr.open('GET',url,true);
         xhr.onload=function(){
         const response=JSON.parse(this.responseText);
           console.log(response)
             if(statusdocument.innerText=="Deleted"){
                 statusdocument.innerText="Active";
                 statusButton.innerText="Delete";
             }
             else{
                 statusdocument.innerText="Deleted";
                 statusButton.innerText="Reverse";
             }
            loadercontainer.classList.remove('active');
         }
         xhr.send(); 
    };
</script>

@endpush