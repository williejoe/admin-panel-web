@extends('admin.app')
@section('content')
<div id="login">
    <div class="container">
      <div
        id="login-row"
        class="row justify-content-center align-items-center"
      >
        <div id="login-column" class="col-md-6">
          <div id="login-box" class="col-md-12">
            <form id="login-form" class="form" action="{{route('login')}}" method="post">
              @csrf
              <h3 class="text-center">Login</h3>
              <div class="form-group">
                <label for="username">Email:</label
                ><br />
                <input
                  type="email"
                  name="email"
                  id="email"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="password">Password:</label
                ><br />
                <input
                  type="password"
                  name="password"
                  id="password"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <input
                  type="submit"
                  name="submit"
                  class="btn btn-md float-right submit__button "
                  value="Login"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('page_scripts')
<script>
    const firebaseConfig = {
          apiKey: "AIzaSyAvxt3QK_a4R5yWB508o0h7t5gmZO5owew",
          authDomain: "laraveladmin-188a3.firebaseapp.com",
          projectId: "laraveladmin-188a3",
          storageBucket: "laraveladmin-188a3.appspot.com",
          messagingSenderId: "255126634021",
          appId: "1:255126634021:web:aac1b4c92696811b7fc9cc",
          measurementId: "G-1GQ5QMT5DJ"
      };
</script>

@endpush